// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class ClipperBackgroundComponent extends StatelessWidget {
  final CustomClipper<Path>? clipper;
  final Color backgroundColor;
  final double height;

  const ClipperBackgroundComponent(
      {super.key,
      this.backgroundColor = AppColors.APP_BAR_BACKGROUND_COLOR,
      this.clipper,
      this.height = 120});

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: clipper ?? OvalBottomBorderClipper(),
      child: Container(
        color: backgroundColor,
        height: height,
      ),
    );
  }
}
