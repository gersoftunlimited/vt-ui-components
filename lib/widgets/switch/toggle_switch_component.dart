// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:toggle_switch/toggle_switch.dart';
import 'package:vt_ui_components/app_colors/app_colors.dart';

// 🌎 Project imports:

class ToggleSwitchComponent extends StatefulWidget {
  List<String> labels;
  int initialLabelIndex;
  List<List<Color>?>? activeBgColors;
  Function(int? index) onIndexChanged;
  double cornerRadius;
  int animationDuration;
  bool animate;
  List<Color> borderColor;
  Color inactiveBackgroundColor;
  List<TextStyle?>? customTextStyles;

  ToggleSwitchComponent(
      {super.key,
      required this.labels,
      required this.onIndexChanged,
      this.initialLabelIndex = 0,
      this.cornerRadius = 50,
      this.activeBgColors,
      this.animate = true,
      this.animationDuration = 250,
      this.borderColor = const [Color(0xff103D4E)],
      this.inactiveBackgroundColor = const Color(0xff103D4E)});

  @override
  State<ToggleSwitchComponent> createState() => _ToggleSwitchComponentState();
}

class _ToggleSwitchComponentState extends State<ToggleSwitchComponent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(child: _childContainer());
  }

  Widget _childContainer() {
    return ToggleSwitch(
      initialLabelIndex: widget.initialLabelIndex,
      minWidth:
          (MediaQuery.of(context).size.width * 0.85 / widget.labels.length),
      cornerRadius: widget.cornerRadius,
      inactiveBgColor: widget.inactiveBackgroundColor,
      customTextStyles: _getCustomTextStyles(),
      totalSwitches: widget.labels.length,
      labels: widget.labels,
      activeBgColors: widget.activeBgColors,
      radiusStyle: true,
      onToggle: (index) {
        widget.onIndexChanged(index);
      },
      borderWidth: 4,
      animate: widget.animate,
      animationDuration: widget.animationDuration,
      borderColor: widget.borderColor,
    );
  }

  _getCustomTextStyles() {
    if (widget.customTextStyles == null) {
      widget.customTextStyles = [];
      for (var i = 0; i < widget.labels.length; i++) {
        widget.customTextStyles!.add(const TextStyle(
            fontWeight: FontWeight.bold, fontSize: 13, color: AppColors.WHITE));
      }
    }

    return widget.customTextStyles;
  }
}
