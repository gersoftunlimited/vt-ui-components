// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class TextSwitchComponent extends StatefulWidget {
  String title;
  TextStyle? titleTextStyle;
  String description;
  TextStyle? descriptionTextStyle;
  Function(bool value) onChanged;
  bool isChecked;
  Color? activeColor;
  bool showOnlyDescriptionIfChecked;
  bool showSwitchRight;
  bool isEnabled;

  TextSwitchComponent(
      {super.key,
      required this.title,
      this.titleTextStyle,
      this.description = "",
      this.descriptionTextStyle,
      required this.onChanged,
      this.isChecked = false,
      this.isEnabled = true,
      this.activeColor = AppColors.SWITCH_ACTIVE_COLOR,
      this.showOnlyDescriptionIfChecked = false,
      this.showSwitchRight = true});

  @override
  State<TextSwitchComponent> createState() => _TextSwitchComponentState();
}

class _TextSwitchComponentState extends State<TextSwitchComponent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AbsorbPointer(
      absorbing: !widget.isEnabled,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: _childContainer()),
    );
  }

  List<Widget> _childContainer() {
    if (widget.showSwitchRight) {
      return [_switchTextContainer(), _switchContainer()];
    } else {
      return [_switchContainer(), _switchTextContainer()];
    }
  }

  _switchContainer() {
    return Container(
        height: 24.0,
        width: 40.0,
        margin: _getSwitchMargins(),
        child: Switch(
          value: widget.isChecked,
          activeColor: widget.activeColor,
          onChanged: (bool value) {
            widget.isChecked = value;
            widget.onChanged(value);
            setState(() {});
          },
        ));
  }

  _getSwitchMargins() {
    return widget.showSwitchRight
        ? const EdgeInsets.only(left: 15)
        : const EdgeInsets.only(right: 15);
  }

  _switchTextContainer() {
    return Expanded(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
          Text(widget.title,
              style: widget.titleTextStyle ?? TextStyles.switchTitleStyle),
          if (_showDescription())
            Container(
                margin: const EdgeInsets.only(top: 10),
                child: Text(widget.description,
                    style: widget.descriptionTextStyle ??
                        TextStyles.switchDescriptionStyle)),
        ]));
  }

  bool _showDescription() {
    if (widget.description.isEmpty) {
      return false;
    } else if (widget.showOnlyDescriptionIfChecked && widget.isChecked) {
      return true;
    } else if (!widget.showOnlyDescriptionIfChecked) {
      return true;
    }

    return false;
  }
}
