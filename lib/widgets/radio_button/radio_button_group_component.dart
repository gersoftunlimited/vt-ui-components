// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class RadioButtonGroupModel {
  String id = "";
  String label = "";

  RadioButtonGroupModel({required this.id, required this.label});
}

class RadioButtonGroupComponent extends StatefulWidget {
  List<RadioButtonGroupModel> options;
  String picked;
  bool isEnabled;
  Function(RadioButtonGroupModel itemSelected) onItemSelected;

  RadioButtonGroupComponent(
      {required this.options,
      required this.picked,
      this.isEnabled = true,
      required this.onItemSelected});
  @override
  State<StatefulWidget> createState() {
    return _RadioButtonGroupComponent();
  }
}

class _RadioButtonGroupComponent extends State<RadioButtonGroupComponent> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      AbsorbPointer(
          absorbing: !widget.isEnabled,
          child: RadioButtonGroup(
            orientation: GroupedButtonsOrientation.VERTICAL,
            labels: widget.options
                .map((option) => option.label)
                .toList()
                .cast<String>(),
            picked: widget.picked,
            onSelected: (String selected) => {
              setState(() {
                _getOptionSelected(selected);
              })
            },
            activeColor: AppColors.PRIMARY_COLOR,
            itemBuilder: (Radio radioButton, Text text, int i) {
              return Row(
                children: [radioButton, Container(child: text)],
              );
            },
          ))
    ]);
  }

  RadioButtonGroupComponent? _getOptionSelected(String label) {
    int index = widget.options.indexWhere(
        (element) => element.label.toLowerCase() == label.toLowerCase());

    if (index != -1) {
      widget.picked = label;
      widget.onItemSelected(widget.options[index]);
    }
  }
}
