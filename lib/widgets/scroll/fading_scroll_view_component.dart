import 'package:fading_edge_scrollview/fading_edge_scrollview.dart';
import 'package:flutter/material.dart';

class FadingScrollViewComponent extends StatefulWidget {
  ScrollController? scrollController;
  EdgeInsets padding;
  Widget child;

  FadingScrollViewComponent(
      {super.key,
      this.scrollController,
      this.padding = EdgeInsets.zero,
      required this.child});

  @override
  State<FadingScrollViewComponent> createState() =>
      _FadingScrollViewComponentState();
}

class _FadingScrollViewComponentState extends State<FadingScrollViewComponent> {
  @override
  Widget build(BuildContext context) {
    if (widget.scrollController == null) {
      return _singleChildScrollViewComponent();
    }
    return FadingEdgeScrollView.fromSingleChildScrollView(
      child: _singleChildScrollViewComponent(),
    );
  }

  _singleChildScrollViewComponent() => SingleChildScrollView(
        controller: widget.scrollController,
        padding: widget.padding,
        child: widget.child,
      );
}
