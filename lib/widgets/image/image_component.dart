// 🎯 Dart imports:
// ignore_for_file: must_be_immutable

import 'dart:convert';
import 'dart:io';

// 🐦 Flutter imports:
import 'package:file_utils/export.dart';
import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:cached_network_image/cached_network_image.dart';
import 'package:internet_file/internet_file.dart';
import 'package:pdfx/pdfx.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';

class ImageComponent extends StatefulWidget {
  String imageFile;
  String semanticLabel;
  double? imageHeight;
  double? imageWidth;
  BoxFit fit;
  bool showDecoration;
  double radius;
  Widget? noPhotoChild;
  Widget? errorWidget;
  Widget? loadingWidget;
  Color? iconColor;

  ImageComponent(
      {super.key,
      this.imageFile = "",
      this.semanticLabel = "",
      this.imageHeight = 150.0,
      this.imageWidth = 150.0,
      this.showDecoration = true,
      this.fit = BoxFit.cover,
      this.radius = 20.0,
      this.iconColor,
      this.noPhotoChild,
      this.errorWidget,
      this.loadingWidget});

  @override
  State<ImageComponent> createState() => _ImageComponentState();
}

class _ImageComponentState extends State<ImageComponent> {
  late PdfController pdfController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (_isPdfNetwork()) {
      pdfController = PdfController(
        document: PdfDocument.openData(InternetFile.get(widget.imageFile)),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.center,
        child: Container(
          height: widget.imageHeight,
          width: widget.imageWidth,
          alignment: Alignment.center,
          decoration: widget.showDecoration
              ? ImageStyles.getInputDecoration(radius: widget.radius)
              : null,
          child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(widget.radius)),
              child: _buildImageContainer()),
        ));
  }

  _buildImageContainer() {
    if (widget.imageFile.isEmpty) return _buildNoPhotoContainer();

    if (_isPdfNetwork()) {
      return _buildPdfNetwork();
    } else if (_isImageNetwork()) {
      return _buildImageNetworkContainer();
    } else if (_isImageAsset()) {
      return _buildImageAssetContainer();
    } else if (_isImageBase64()) {
      return _buildImageBase64Container();
    }

    return _buildImageFileContainer();
  }

  _buildNoPhotoContainer() => SizedBox(
        height: widget.imageHeight ?? 150.0,
        width: widget.imageWidth ?? 150.0,
        child: widget.noPhotoChild ??
            const Icon(
              Icons.no_photography_outlined,
              color: AppColors.GREY,
              size: 50.0,
            ),
      );

  _buildImageNetworkContainer() => Semantics(
      label: widget.semanticLabel,
      child: CachedNetworkImage(
          key: Key(widget.imageFile),
          imageUrl: widget.imageFile,
          height: widget.imageHeight,
          width: widget.imageWidth,
          repeat: ImageRepeat.noRepeat,
          fit: widget.fit,
          colorBlendMode: BlendMode.color,
          fadeInDuration: const Duration(seconds: 0),
          fadeOutDuration: const Duration(seconds: 0),
          errorWidget: (context, url, error) => _errorBuilder(),
          progressIndicatorBuilder: (context, url, progress) =>
              widget.loadingWidget ?? LoadingComponent()));

  _buildPdfNetwork() => PdfView(
        controller: pdfController,
        scrollDirection: Axis.vertical,
      );

  _buildImageFileContainer() => Semantics(
      label: widget.semanticLabel,
      child: Container(
          height: widget.imageHeight,
          width: widget.imageWidth,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              color: AppColors.WHITE,
              image: DecorationImage(
                  image: Image.file(
                    File(widget.imageFile),
                    errorBuilder: (context, error, stackTrace) =>
                        _errorBuilder(),
                  ).image,
                  fit: widget.fit))));

  _buildImageAssetContainer() => Semantics(
      label: widget.semanticLabel,
      child: SizedBox(
          height: widget.imageHeight,
          width: widget.imageWidth,
          child: Image.asset(
            widget.imageFile,
            fit: widget.fit,
            errorBuilder: (context, error, stackTrace) => _errorBuilder(),
            color: widget.iconColor,
          )));

  bool _isImageNetwork() {
    return ImageUtils.isImageNetwork(widget.imageFile);
  }

  bool _isPdfNetwork() {
    return widget.imageFile.startsWith("http") &&
        widget.imageFile.endsWith(".pdf");
  }

  bool _isImageAsset() {
    return ImageUtils.isImageAsset(widget.imageFile);
  }

  _errorBuilder() =>
      widget.errorWidget ??
      const Center(
        child: Icon(
          Icons.error,
          size: 50,
          color: AppColors.PRIMARY_COLOR,
        ),
      );

  bool _isImageBase64() {
    return ImageUtils.isImageBase64(widget.imageFile);
  }

  _buildImageBase64Container() => Semantics(
      label: widget.semanticLabel,
      child: Container(
          height: widget.imageHeight,
          width: widget.imageWidth,
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              color: AppColors.WHITE,
              image: DecorationImage(
                  image: Image.memory(
                    base64Decode(widget.imageFile),
                    errorBuilder: (context, error, stackTrace) =>
                        _errorBuilder(),
                  ).image,
                  fit: widget.fit))));
}
