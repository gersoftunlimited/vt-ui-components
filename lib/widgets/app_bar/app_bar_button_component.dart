// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

// 🐦 Flutter imports:
import 'package:flutter/material.dart';

class VTUIAppBarButtonComponent extends StatelessWidget {
  Widget icon;
  String tooltip;
  Function() onPressed;

  VTUIAppBarButtonComponent(
      {super.key,
      required this.icon,
      required this.onPressed,
      this.tooltip = ""});

  @override
  Widget build(BuildContext context) {
    return IconButton(
        padding: EdgeInsets.zero,
        constraints: const BoxConstraints(),
        splashRadius: 20.0,
        tooltip: tooltip,
        icon: icon,
        onPressed: () {
          onPressed();
        });
  }
}
