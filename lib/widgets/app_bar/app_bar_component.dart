// ignore_for_file: must_be_immutable

// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class VTUIAppBarComponent extends StatefulWidget implements PreferredSizeWidget {
  BuildContext context;
  Color backgroundColor;
  Color backIconColor;
  bool showMiddleLogo;
  Widget? middleChild;
  bool automaticallyImplyLeading;
  List<Widget> rightIcons;
  bool centerTitle;

  VTUIAppBarComponent(
      {super.key,
      required this.context,
      this.backgroundColor = AppColors.APP_BAR_BACKGROUND_COLOR,
      this.backIconColor = AppColors.PRIMARY_COLOR,
      this.rightIcons = const [],
      this.showMiddleLogo = true,
      this.automaticallyImplyLeading = true,
      this.middleChild,
      this.preferredSize = const Size.fromHeight(65),
      this.centerTitle = true});

  @override
  State<VTUIAppBarComponent> createState() => _VTUIAppBarComponentState();

  @override
  Size preferredSize;
}

class _VTUIAppBarComponentState extends State<VTUIAppBarComponent> {
  final int MAX_RIGHT_ICONS_SIZE = 2;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: widget.backgroundColor,
      automaticallyImplyLeading: widget.automaticallyImplyLeading,
      centerTitle: widget.centerTitle,
      iconTheme: IconThemeData(
        color: widget.backIconColor, //change your color here
      ),
      titleSpacing: 10,
      elevation: 0,
      title: _appNameImage(),
      actions: [_rightIcons()],
    );
  }

  _appNameImage() {
    return widget.showMiddleLogo
        ? Image.asset(
            'assets/images/app_name.png',
            width: 150,
          )
        : (widget.middleChild ?? Container());
  }

  Widget _rightIcons() {
    return Container(
        margin: const EdgeInsets.only(right: 20),
        alignment: Alignment.centerRight,
        child: Wrap(
            spacing: 15,
            children: widget.rightIcons.take(MAX_RIGHT_ICONS_SIZE).toList()));
  }
}
