// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:fluttertoast/fluttertoast.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

enum ToastTypes { standard, error, success }

class ToastUtils {
  static displayToast(String message,
      {long = false, ToastTypes type = ToastTypes.standard}) {
    if (message == null || message == '') {
      return;
    }

    var toastStyle = _getToastStyleByType(type: type);

    Fluttertoast.showToast(
        msg: message,
        toastLength: long ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: toastStyle['background'],
        textColor: toastStyle['textColor'],
        fontSize: 16.0);
  }

  static dynamic _getToastStyleByType({required ToastTypes type}) {
    switch (type) {
      case ToastTypes.error:
        return {'background': AppColors.RED, 'textColor': AppColors.WHITE};
      case ToastTypes.success:
        return {'background': AppColors.GREEN, 'textColor': AppColors.WHITE};
      default:
        return {'background': AppColors.WHITE, 'textColor': AppColors.BLACK};
    }
  }

  static Widget _modifiedToast() {
    bool _visible = false;
    return AnimatedOpacity(
      // If the widget is visible, animate to 0.0 (invisible).
      // If the widget is hidden, animate to 1.0 (fully visible).
      opacity: _visible ? 1.0 : 0.0,
      duration: const Duration(milliseconds: 500),
      // The green box must be a child of the AnimatedOpacity widget.
      child: Container(
        width: 200.0,
        height: 200.0,
        color: Colors.green,
      ),
    );
  }
}
