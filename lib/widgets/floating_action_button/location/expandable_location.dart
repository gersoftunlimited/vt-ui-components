import 'package:flutter/material.dart';
import 'package:flutter_expandable_fab/flutter_expandable_fab.dart';

class ExpandableLocation {
    static FloatingActionButtonLocation get locations => ExpandableFab.location;
}