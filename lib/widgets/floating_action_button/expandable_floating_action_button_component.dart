import 'package:flutter/material.dart';
import 'package:flutter_expandable_fab/flutter_expandable_fab.dart';
import 'package:vt_ui_components/export.dart';

class ExpandableFloatingActionButtonComponent extends StatelessWidget {
  List<ExpandableOption> children;
  Key? key;

  ExpandableFloatingActionButtonComponent({super.key, required this.children});

  @override
  Widget build(BuildContext context) {
    return ExpandableFab(
      key: key,
      type: ExpandableFabType.up,
      distance: 50,
      backgroundColor: AppColors.PRIMARY_COLOR,
      child: const Icon(Icons.add_rounded),
      // ignore: prefer_const_constructors
      closeButtonStyle: ExpandableFabCloseButtonStyle(
          backgroundColor: AppColors.PRIMARY_COLOR,
          child: const Icon(Icons.close_rounded)),
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 20),
          child: Wrap(
              runAlignment: WrapAlignment.end,
              alignment: WrapAlignment.end,
              crossAxisAlignment: WrapCrossAlignment.end,
              spacing: 20,
              direction: Axis.vertical,
              children: children),
        )
      ],
    );
  }
}
