import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

class ExpandableOption extends StatelessWidget {
  String text;
  Function onTap;
  Color? backgroundColor;

  ExpandableOption(
      {super.key,
      required this.text,
      required this.onTap,
      this.backgroundColor});

  // Styles
  late BoxDecoration boxDecoration = BoxDecoration(
      color: backgroundColor ?? AppColors.PRIMARY_COLOR,
      borderRadius: const BorderRadius.all(Radius.circular(10)));

  final EdgeInsets padding =
      const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10);

  final TextStyle textStyle = const TextStyle(
      color: AppColors.WHITE, fontSize: 16, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) => GestureDetector(
      onTap: () => onTap(),
      child: Container(
        decoration: boxDecoration,
        padding: padding,
        child: Text(
          text,
          style: textStyle,
        ),
      ));
}
