// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:flutter_spinkit/flutter_spinkit.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';

class LoadingComponent extends StatelessWidget {
  Color color;
  Widget? child;
  double size;
  Alignment alignment;

  LoadingComponent(
      {super.key,
      this.color = AppColors.PRIMARY_COLOR,
      this.child,
      this.size = 35.0,
      this.alignment = Alignment.topCenter});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        runAlignment: WrapAlignment.center,
        alignment: WrapAlignment.center,
        children: [_loadingComponent(), _getTextChild()],
      ),
    );
  }

  _loadingComponent() {
    return Container(
        margin: MarginStyles.getLoadingMargins(),
        width: size,
        height: size,
        child: SpinKitDoubleBounce(
          color: color,
        ));
  }

  _getTextChild() {
    return child != null
        ? Container(
            width: double.infinity, alignment: Alignment.center, child: child)
        : Container();
  }
}
