// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';

class NoResultsComponent extends StatelessWidget {

  Widget child;
  Widget? icon;
  Alignment? alignment;

  NoResultsComponent(
      {super.key, required this.child, this.icon, this.alignment});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 40, left: 20, right: 20),
      alignment: alignment ?? Alignment.center,
      child: Align(
        alignment: alignment ?? Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            child,
            const SizedBox(height: 20),
            icon ??
                const Icon(Icons.search_off,
                    color: AppColors.PRIMARY_COLOR, size: 65)
          ],
        ),
      ),
    );
  }
}
