// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class TextCheckboxComponent extends StatefulWidget {
  Widget? child;
  TextStyle? titleTextStyle;
  String description;
  TextStyle? descriptionTextStyle;
  Function(bool value) onChanged;
  bool isChecked;
  Color? activeColor;
  bool showOnlyDescriptionIfChecked;
  bool showCheckboxRight;
  double height;
  double width;
  double margins;

  TextCheckboxComponent(
      {super.key,
      this.child,
      this.titleTextStyle,
      this.description = "",
      this.descriptionTextStyle,
      required this.onChanged,
      this.isChecked = false,
      this.height = 24.0,
      this.width = 24.0,
      this.activeColor = AppColors.GREEN,
      this.showOnlyDescriptionIfChecked = false,
      this.showCheckboxRight = true,
      this.margins = 15.0});

  @override
  State<TextCheckboxComponent> createState() => _TextCheckboxComponentState();
}

class _TextCheckboxComponentState extends State<TextCheckboxComponent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: _childContainer(),
    );
  }

  List<Widget> _childContainer() {
    if (widget.showCheckboxRight) {
      return [_checkboxTextContainer(), _checkboxContainer()];
    } else {
      return [_checkboxContainer(), _checkboxTextContainer()];
    }
  }

  _checkboxContainer() {
    return Container(
        height: widget.height,
        width: widget.height,
        margin: _getCheckboxMargins(),
        child: Checkbox(
          value: widget.isChecked,
          activeColor: widget.activeColor,
          onChanged: (bool? value) {
            if (value == null) return;

            widget.isChecked = value;
            widget.onChanged(value);
            setState(() {});
          },
        ));
  }

  _getCheckboxMargins() {
    return widget.showCheckboxRight
        ? EdgeInsets.only(left: widget.margins)
        : EdgeInsets.only(right: widget.margins);
  }

  _checkboxTextContainer() {
    if (widget.child == null) return Container();

    return Expanded(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
          widget.child!,
          if (_showDescription())
            Container(
                margin: const EdgeInsets.only(top: 10),
                child: Text(widget.description,
                    style: widget.descriptionTextStyle ??
                        TextStyles.checkboxDescriptionStyle)),
        ]));
  }

  bool _showDescription() {
    if (widget.description.isEmpty) {
      return false;
    } else if (widget.showOnlyDescriptionIfChecked && widget.isChecked) {
      return true;
    } else if (!widget.showOnlyDescriptionIfChecked) {
      return true;
    }

    return false;
  }
}
