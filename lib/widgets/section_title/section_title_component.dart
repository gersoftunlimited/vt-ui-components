// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class SectionTitleCompoent extends StatelessWidget {
  String title;
  SectionTitleCompoent({super.key, required this.title});

  @override
  Widget build(BuildContext context) {
    return Text(
      title.toUpperCase(),
      style: const TextStyle(fontWeight: FontWeight.bold),
    );
  }
}
