// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:utils/export.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';

class HideKeyboardContainer extends StatefulWidget {
  Widget child;
  Color backgroundColor;
  EdgeInsets margins;

  HideKeyboardContainer(
      {super.key,
      required this.child,
      this.backgroundColor = AppColors.WHITE,
      this.margins = EdgeInsets.zero});

  @override
  State<HideKeyboardContainer> createState() => _HideKeyboardContainerState();
}

class _HideKeyboardContainerState extends State<HideKeyboardContainer> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          KeyboardUtils.hideKeyboard();
          FocusUtils.unfocusAll(context: context);
        },
        child: Container(
          margin: widget.margins,
          decoration: BoxDecoration(color: widget.backgroundColor),
          child: widget.child,
        ));
  }
}
