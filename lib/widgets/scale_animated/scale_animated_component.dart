// 🐦 Flutter imports:
import 'package:flutter/material.dart';

class ScaleAnimatedComponent extends StatelessWidget {
  Widget child;
  double scale;

  ScaleAnimatedComponent({super.key, required this.child, required this.scale});

  @override
  Widget build(BuildContext context) {
    return AnimatedScale(
        scale: scale,
        duration: const Duration(milliseconds: 200),
        child: child);
  }
}
