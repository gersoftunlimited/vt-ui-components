// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

class SearchInputComponent extends StatefulWidget {
  String initialValue;
  String hint;
  TextInputType keyboardType;
  Widget? prefixIcon;
  Widget? suffixIcon;
  Function(String value) onChanged;
  bool isEnabled;
  int? maxLines;
  int? maxLength;
  Widget? rightChild;
  TextEditingController? textEditingController;
  bool showHintInLabel;
  bool showHintWhenFocus;
  FocusNode? focusNode;
  Function? onFieldSubmitted;
  TextInputAction textInputAction;

  SearchInputComponent(
      {super.key,
      this.initialValue = "",
      this.hint = "",
      this.prefixIcon,
      this.suffixIcon,
      this.keyboardType = TextInputType.text,
      this.isEnabled = true,
      this.maxLines,
      this.maxLength,
      this.rightChild,
      this.textEditingController,
      required this.onChanged,
      this.showHintInLabel = true,
      this.showHintWhenFocus = false,
      this.focusNode,
      this.onFieldSubmitted,
      this.textInputAction = TextInputAction.next});

  @override
  State<SearchInputComponent> createState() => _SearchInputComponentState();
}

class _SearchInputComponentState extends State<SearchInputComponent> {
  @override
  void initState() {
    super.initState();

    widget.textEditingController ?? TextEditingController();

    if (widget.initialValue.isNotEmpty) {
      widget.textEditingController?.text = widget.initialValue;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
              child: TextFormField(
            keyboardType: widget.keyboardType,
            autocorrect: false,
            controller: widget.textEditingController,
            enabled: widget.isEnabled,
            maxLines: widget.maxLines ?? 1,
            maxLength: widget.maxLength,
            focusNode: widget.focusNode,
            textInputAction: widget.textInputAction,
            onChanged: (value) {
              widget.onChanged(value);
            },
            onFieldSubmitted: (value) {
              if (widget.onFieldSubmitted != null) {
                widget.onFieldSubmitted!();
              }
            },
            style: const TextStyle(
              fontSize: 16,
            ),
            decoration: InputStyles.getInputDecoration(
                hint: widget.hint,
                prefixIcon: widget.prefixIcon,
                suffixIcon: widget.suffixIcon,
                showHintInLabel: widget.showHintInLabel,
                showHintWhenFocus: widget.showHintWhenFocus),
          )),
          if (widget.rightChild != null) widget.rightChild!
        ]);
  }
}
