// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 🌎 Project imports:
import 'package:utils/string/string_utils.dart';
import 'package:vt_ui_components/export.dart';

class SearchModel {
  const SearchModel(
      {required this.id, required this.name, this.description = "", this.icon});

  final Widget? icon;
  final String id;
  final String name;
  final String description;

  @override
  String toString() {
    return '$name, $description';
  }

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != runtimeType) {
      return false;
    }
    return other is SearchModel &&
        other.id == id &&
        other.name == name &&
        other.description == description;
  }

  @override
  int get hashCode => Object.hash(id, name, description);
}

class SearchComponent extends StatefulWidget {
  List<SearchModel> options;
  Function(String value) onChanged;
  Function(String value) onSelected;
  Function(SearchModel)? onSearchModelSelected;

  String hint;
  double suggestionRowHeight;
  Color backgroundColor;
  TextInputAction textInputAction;
  String initialValue;
  bool showHintWhenFocus;
  bool showHintInLabel;
  bool showClearButton;

  FocusNode? focus;

  SearchComponent(
      {super.key,
      this.options = const [],
      required this.onChanged,
      required this.onSelected,
      this.onSearchModelSelected,
      required this.hint,
      this.backgroundColor = AppColors.WHITE,
      this.suggestionRowHeight = 68,
      this.showHintWhenFocus = true,
      this.showHintInLabel = false,
      this.showClearButton = false,
      this.textInputAction = TextInputAction.done,
      this.initialValue = "",
      this.focus});

  @override
  State<SearchComponent> createState() => _SearchComponentState();

  static String _displayStringForOption(SearchModel option) => option.name;

  static String _displayDescriptionForOption(SearchModel option) =>
      option.description;

  static Widget _displayIconForOption(SearchModel option) =>
      option.icon ?? Container();
}

class _SearchComponentState extends State<SearchComponent> {
  final String TAG = "search";

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: RawAutocomplete<SearchModel>(
        optionsBuilder: (TextEditingValue textEditingValue) {
          if (textEditingValue.text == '') {
            return const Iterable<SearchModel>.empty();
          }

          return widget.options.where((SearchModel option) {
            return StringUtils.format(
                        text: option.name.toLowerCase(),
                        type: StringType.removeDiacritics)
                    .contains(StringUtils.format(
                        text: textEditingValue.text.toLowerCase(),
                        type: StringType.removeDiacritics)) ||
                StringUtils.format(
                        text: option.description.toLowerCase(),
                        type: StringType.removeDiacritics)
                    .contains(StringUtils.format(
                        text: textEditingValue.text.toLowerCase(),
                        type: StringType.removeDiacritics));
          });
        },
        displayStringForOption: (SearchModel option) =>
            SearchComponent._displayStringForOption(option),
        fieldViewBuilder:
            (context, textEditingController, focusNode, onFieldSubmitted) =>
                _buildTextField(
                    controller: textEditingController,
                    focusNode: focusNode,
                    onFieldSubmitted: onFieldSubmitted,
                    textInputAction: widget.textInputAction),
        optionsViewBuilder: (context, onSelected, options) =>
            _buildOptionsViewBuilder(
                context: context, onSelected: onSelected, options: options),
        onSelected: (SearchModel selection) {
          widget.onSelected(selection.id);
          if (widget.onSearchModelSelected != null) {
            widget.onSearchModelSelected!(selection);
          }
        },
      ),
    );
  }

  _buildTextField(
      {required TextEditingController controller,
      required FocusNode focusNode,
      required Function() onFieldSubmitted,
      TextInputAction textInputAction = TextInputAction.done}) {
    return SearchInputComponent(
      onChanged: (String value) {
        widget.onChanged(value);
      },
      initialValue: widget.initialValue,
      textEditingController: controller,
      showHintInLabel: widget.showHintInLabel,
      showHintWhenFocus: widget.showHintWhenFocus,
      hint: widget.hint,
      focusNode: focusNode,
      onFieldSubmitted: onFieldSubmitted,
      textInputAction: textInputAction,
    );
  }

  _buildOptionsViewBuilder(
          {required BuildContext context,
          required AutocompleteOnSelected<SearchModel> onSelected,
          required Iterable<SearchModel> options}) =>
      Align(
        alignment: Alignment.topLeft,
        child: Container(
            margin: const EdgeInsets.only(right: 10, left: 10, top: 10),
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width * 0.85,
            ),
            child: Material(
              elevation: 2.0,
              child: SizedBox(
                height: _getOptionsItemHeight(options: options),
                child: ListView.builder(
                  padding: const EdgeInsets.all(8.0),
                  itemCount: options.length,
                  itemBuilder: (BuildContext context, int index) {
                    final SearchModel option = options.elementAt(index);
                    return _itemBuilder(
                        onSelected: onSelected, option: option);
                  },
                ),
              ),
            )),
      );

  _itemBuilder(
          {required Function(SearchModel) onSelected,
          required SearchModel option}) =>
      GestureDetector(
        onTap: () {
          onSelected(option);
        },
        child: ListTile(
            title: Row(
          children: [
            SearchComponent._displayIconForOption(option),
            Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  Text(
                    SearchComponent._displayStringForOption(option),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 5),
                    child: Text(
                      SearchComponent._displayDescriptionForOption(option),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(fontSize: 14),
                    ),
                  )
                ])),
          ],
        )),
      );

  _getOptionsItemHeight({required Iterable<SearchModel> options}) {
    if (options.isEmpty) return 0.0;

    return options.length > 3
        ? (widget.suggestionRowHeight * 3)
        : options.length * widget.suggestionRowHeight;
  }
}
