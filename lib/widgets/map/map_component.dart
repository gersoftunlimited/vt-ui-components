import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:latlong2/latlong.dart';
import 'package:vt_ui_components/widgets/map/constants/map_tile_layer_constants.dart';
import 'package:vt_ui_components/widgets/map/model/map_location_model.dart';
import 'package:vt_ui_components/widgets/map/model/map_model.dart';

class MapComponent extends StatefulWidget {
  List<MapModel> items;
  MapLocationModel center;
  double zoom;
  double height;
  double? width;
  EdgeInsets margin;
  double radius;

  Function(MapModel item) onItemPressed;

  MapComponent(
      {super.key,
      required this.center,
      required this.items,
      this.zoom = 10,
      this.height = 300,
      this.width,
      this.radius = 10,
      this.margin = const EdgeInsets.only(left: 10, right: 10),
      required this.onItemPressed});

  @override
  State<MapComponent> createState() => _MapComponentState();
}

class _MapComponentState extends State<MapComponent> {
  MapController mapController = MapController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: widget.margin,
        width: widget.width,
        height: widget.height,
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(widget.radius)),
            child: _mapView()));
  }

  _mapView() {
    return Listener(
        onPointerUp: (pointer) {
          setState(() {
            FocusScope.of(context).unfocus();
          });
        },
        child: Semantics(
            child: FlutterMap(
          mapController: mapController,
          options: MapOptions(
              center: LatLng(widget.center.latitude, widget.center.longitude),
              zoom: widget.zoom,
              onMapCreated: (onMapCreated) => {
                    onMapCreated.onReady.then((value) => {setState(() {})})
                  },
              minZoom: 3,
              maxZoom: 20,
              plugins: [
                MarkerClusterPlugin(),
              ],
              onPositionChanged: (MapPosition position, bool changed) async {
                widget.zoom = mapController.zoom;
              }),
          layers: [
            MapTileLayerConstants.GOOGLE_MAP,
            MarkerClusterLayerOptions(
              maxClusterRadius: 60,
              disableClusteringAtZoom: 100,
              showPolygon: false,
              rotate: true,
              rotateAlignment: AlignmentDirectional.center,
              fitBoundsOptions: const FitBoundsOptions(
                padding: EdgeInsets.all(20),
              ),
              markers: widget.items
                  .map((item) => Marker(
                        point: LatLng(
                            item.location.latitude, item.location.longitude),
                        builder: (context) => const Icon(Icons.location_on),
                      ))
                  .toList(),
              size: const Size(45, 45),
              onClusterTap: (cluster) {
                Future.delayed(const Duration(seconds: 1), () {
                  setState(() {});
                });
              },
              builder: (context, markersFluster) {
                return const Icon(Icons.circle);
              },
            ),
          ],
        )));
  }
}
