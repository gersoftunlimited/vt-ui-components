// ignore_for_file: non_constant_identifier_names

import 'package:flutter_map/flutter_map.dart';

class MapTileLayerConstants {
  static TileLayerOptions GOOGLE_MAP = TileLayerOptions(
      minZoom: 1,
      maxZoom: 21,
      urlTemplate:
          "http://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga");
}
