class MapLocationModel {
  double latitude = 0;
  double longitude = 0;

  MapLocationModel({required this.latitude, required this.longitude});
}
