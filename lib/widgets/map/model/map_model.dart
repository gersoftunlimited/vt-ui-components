import 'package:vt_ui_components/widgets/map/model/map_location_model.dart';

class MapModel {
  String id = "";
  String name = "";
  String description = "";

  MapLocationModel location;

  MapModel(
      {this.id = "",
      this.name = "",
      this.description = "",
      required this.location});
}
