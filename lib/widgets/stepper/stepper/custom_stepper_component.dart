// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';
import 'package:vt_ui_components/widgets/stepper/controller/custom_stepper_controller.dart';

class CustomStepperComponent extends StatefulWidget {
  CustomStepperController controller;
  Color color;
  double? width;

  CustomStepperComponent(
      {super.key,
      required this.controller,
      this.color = AppColors.PRIMARY_COLOR,
      this.width});

  @override
  State<CustomStepperComponent> createState() => _CustomStepperComponentState();
}

class _CustomStepperComponentState extends State<CustomStepperComponent> {
  late Size size;

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() => setState(() => {}));
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return SizedBox(
      width: widget.width ?? size.width,
      height: 10.0,
      child: Stack(
        children: <Widget>[
          Container(
            width: widget.width ?? size.width,
            decoration: const BoxDecoration(
                color: AppColors.LIGHT_GREY,
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
          ),
          AnimatedContainer(
            duration: const Duration(milliseconds: 500),
            width: (widget.width ?? size.width) *
                (widget.controller.page / widget.controller.totalPages),
            decoration: const BoxDecoration(
                color: AppColors.PRIMARY_COLOR,
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
          )
        ],
      ),
    );
  }
}
