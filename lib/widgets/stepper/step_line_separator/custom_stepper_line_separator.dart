// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';
import 'package:vt_ui_components/widgets/stepper/controller/custom_stepper_controller.dart';

class CustomStepperLineSeparatorComponent extends StatefulWidget {
  CustomStepperController controller;
  int page;

  CustomStepperLineSeparatorComponent({super.key, required this.controller, required this.page});

  @override
  State<CustomStepperLineSeparatorComponent> createState() => _CustomStepperLineSeparatorComponentState();
}

class _CustomStepperLineSeparatorComponentState extends State<CustomStepperLineSeparatorComponent> {
  late Size size;

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return Container(
      alignment: Alignment.topLeft,
      width: (size.width / widget.controller.totalPages) * 0.6,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 500),
            margin: const EdgeInsets.only(left: 5.0, right: 5.0),
            height: 2.0,
            // TODO: The short line has to be shown only when the bubble is active, when not, the width must be 0 (double condition)=
            width:  isActive() ?  (size.width / widget.controller.totalPages) * 0.6 : 20.0,
            decoration: BoxDecoration(
                color: isActive()
                    ? AppColors.PRIMARY_COLOR
                    : AppColors.GREY),
          ),
    );
  }

  bool isActive() =>  widget.controller.page > widget.page ||
      widget.controller.page == widget.page;
}
