// 🌎 Project imports:
import 'package:flutter/material.dart';

class CustomStepperController with ChangeNotifier {
  int page;
  int totalPages;

  CustomStepperController({required this.page, required this.totalPages}) {
    totalPages--;
  }

  void nextPage() {
    page++;
    notifyListeners();
  }
}
