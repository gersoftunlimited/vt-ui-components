// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:fading_edge_scrollview/fading_edge_scrollview.dart';
import 'package:intl/date_symbol_data_local.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';
import 'package:vt_ui_components/widgets/scrollable_calendar/gestures/tap.dart';
import 'package:vt_ui_components/widgets/scrollable_calendar/scrollable_calendar_widget.dart';
import 'extra/style.dart';

class ScrollableCalendarDatePickerComponent extends StatefulWidget {
  /// Start Date in case user wants to show past dates
  /// If not provided calendar will start from the initialSelectedDate
  final DateTime startDate;

  /// Width of the selector
  final double width;

  /// Height of the selector
  final double height;

  /// Text color for the selected Date
  final Color selectedTextColor;

  /// Background color for the selector
  final Color selectionColor;

  /// Text Color for the deactivated dates
  final Color deactivatedColor;

  /// TextStyle for Month Value
  final TextStyle monthTextStyle;

  /// TextStyle for day Value
  final TextStyle dayTextStyle;

  /// TextStyle for the date Value
  final TextStyle dateTextStyle;

  /// Current Selected Date
  final DateTime? /*?*/ initialSelectedDate;

  /// Contains the list of inactive dates.
  /// All the dates defined in this List will be deactivated
  final List<DateTime>? inactiveDates;

  /// Contains the list of active dates.
  /// Only the dates in this list will be activated.
  final List<DateTime>? activeDates;

  /// Callback function for when a different date is selected
  final DateChangeListener? onDateChange;

  /// Max limit up to which the dates are shown.
  /// Days are counted from the startDate
  final int daysCount;

  /// Locale for the calendar default: en_us
  final String locale;

  const ScrollableCalendarDatePickerComponent({
    super.key,
    required this.startDate,
    this.width = 60,
    this.height = 80,
    this.monthTextStyle = defaultMonthTextStyle,
    this.dayTextStyle = defaultDayTextStyle,
    this.dateTextStyle = defaultDateTextStyle,
    this.selectedTextColor = Colors.white,
    this.selectionColor = AppColors.PRIMARY_COLOR,
    this.deactivatedColor = AppColors.PRIMARY_COLOR,
    this.initialSelectedDate,
    this.activeDates,
    this.inactiveDates,
    this.daysCount = 500,
    this.onDateChange,
    this.locale = "en_US",
  }) : assert(
            activeDates == null || inactiveDates == null,
            "Can't "
            "provide both activated and deactivated dates List at the same time.");

  @override
  State<StatefulWidget> createState() =>
      _ScrollableCalendarDatePickerComponentState();
}

class _ScrollableCalendarDatePickerComponentState
    extends State<ScrollableCalendarDatePickerComponent> {
  DateTime? _currentDate;

  final ScrollController _itemScrollController = ScrollController();

  late final TextStyle selectedDateStyle;
  late final TextStyle selectedMonthStyle;
  late final TextStyle selectedDayStyle;

  late final TextStyle deactivatedDateStyle;
  late final TextStyle deactivatedMonthStyle;
  late final TextStyle deactivatedDayStyle;

  DatePickerController datePickerController = DatePickerController();

  List<DateTime> dates = [];

  @override
  void initState() {
    initializeDateFormatting(widget.locale, null);

    datePickerController.setDatePickerState(this);

    selectedDateStyle =
        widget.dateTextStyle.copyWith(color: widget.selectedTextColor);
    selectedMonthStyle =
        widget.monthTextStyle.copyWith(color: widget.selectedTextColor);
    selectedDayStyle =
        widget.dayTextStyle.copyWith(color: widget.selectedTextColor);

    deactivatedDateStyle =
        widget.dateTextStyle.copyWith(color: widget.deactivatedColor);
    deactivatedMonthStyle =
        widget.monthTextStyle.copyWith(color: widget.deactivatedColor);
    deactivatedDayStyle =
        widget.dayTextStyle.copyWith(color: widget.deactivatedColor);

    for (var i = 0; i < widget.daysCount; i++) {
      DateTime date = widget.startDate
          .add(Duration(days: i, hours: 0, minutes: 0, seconds: 0));

      dates.add(DateTime(date.year, date.month, date.day, 0, 0, 0));
    }

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scrollToToday();
    });

    super.initState();
  }

  _scrollToToday() {
    DateTime firstDateToBeShown = _findDate(DateTime(
        DateTime.now().year, DateTime.now().month, DateTime.now().day - 2));
    _scrollToDate(firstDateToBeShown);

    DateTime todayDate = _findDate(DateTime(
        DateTime.now().year, DateTime.now().month, DateTime.now().day));
    _currentDate = todayDate;
  }

  _findDate(DateTime dateTime) {
    return dates.firstWhere((element) => element == dateTime);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: widget.height,
        width: MediaQuery.of(context).size.width,
        child: FadingEdgeScrollView.fromScrollView(
          child: ListView.builder(
            controller: _itemScrollController,
            itemCount: dates.length,
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              DateTime date = dates[index];
              bool isDeactivated = false;

              if (widget.inactiveDates != null) {
                for (DateTime inactiveDate in widget.inactiveDates!) {
                  if (_compareDate(date, inactiveDate)) {
                    isDeactivated = true;
                    break;
                  }
                }
              }

              if (widget.activeDates != null) {
                isDeactivated = true;
                for (DateTime activateDate in widget.activeDates!) {
                  // Compare the date if it is in the
                  if (_compareDate(date, activateDate)) {
                    isDeactivated = false;
                    break;
                  }
                }
              }

              bool isSelected = _currentDate != null
                  ? _compareDate(date, _currentDate!)
                  : false;

              return ScrollableCalendarWidget(
                date: date,
                monthTextStyle: isDeactivated
                    ? deactivatedMonthStyle
                    : isSelected
                        ? selectedMonthStyle
                        : widget.monthTextStyle,
                dateTextStyle: isDeactivated
                    ? deactivatedDateStyle
                    : isSelected
                        ? selectedDateStyle
                        : widget.dateTextStyle,
                dayTextStyle: isDeactivated
                    ? deactivatedDayStyle
                    : isSelected
                        ? selectedDayStyle
                        : widget.dayTextStyle,
                width: widget.width,
                locale: widget.locale,
                selectionColor:
                    isSelected ? widget.selectionColor : Colors.transparent,
                onDateSelected: (selectedDate) {
                  if (isDeactivated) return;

                  if (widget.onDateChange != null) {
                    widget.onDateChange!(selectedDate);
                  }
                  setState(() {
                    _currentDate = selectedDate;
                  });

                  _scrollToDate(DateTime(_currentDate!.year,
                      _currentDate!.month, _currentDate!.day - 2));
                },
              );
            },
          ),
        ));
  }

  _scrollToDate(DateTime dateTime) {
    datePickerController.animateToDate(dateTime);
  }

  bool _compareDate(DateTime date1, DateTime date2) {
    return date1.day == date2.day &&
        date1.month == date2.month &&
        date1.year == date2.year;
  }
}

class DatePickerController {
  _ScrollableCalendarDatePickerComponentState? _datePickerState;

  void setDatePickerState(_ScrollableCalendarDatePickerComponentState state) {
    _datePickerState = state;
  }

  void jumpToSelection() {
    assert(_datePickerState != null,
        'DatePickerController is not attached to any DatePicker View.');

    _datePickerState!._itemScrollController
        .jumpTo(_calculateDateOffset(_datePickerState!._currentDate!));
  }

  void animateToSelection(
      {duration = const Duration(milliseconds: 500), curve = Curves.linear}) {
    assert(_datePickerState != null,
        'DatePickerController is not attached to any DatePicker View.');

    _datePickerState!._itemScrollController.animateTo(
        _calculateDateOffset(_datePickerState!._currentDate!),
        duration: duration,
        curve: curve);
  }

  void animateToDate(DateTime date,
      {duration = const Duration(milliseconds: 500), curve = Curves.linear}) {
    assert(_datePickerState != null,
        'DatePickerController is not attached to any DatePicker View.');

    _datePickerState!._itemScrollController.animateTo(
        _calculateDateOffset(date),
        duration: duration,
        curve: curve);
  }

  void setDateAndAnimate(DateTime date,
      {duration = const Duration(milliseconds: 500), curve = Curves.linear}) {
    assert(_datePickerState != null,
        'DatePickerController is not attached to any DatePicker View.');

    _datePickerState!._itemScrollController.animateTo(
        _calculateDateOffset(date),
        duration: duration,
        curve: curve);

    if (date.compareTo(_datePickerState!.widget.startDate) >= 0 &&
        date.compareTo(_datePickerState!.widget.startDate
                .add(Duration(days: _datePickerState!.widget.daysCount))) <=
            0) {
      _datePickerState!._currentDate = date;
    }
  }

  double _calculateDateOffset(DateTime date) {
    final startDate = DateTime(
        _datePickerState!.widget.startDate.year,
        _datePickerState!.widget.startDate.month,
        _datePickerState!.widget.startDate.day);

    int offset = date.difference(startDate).inDays;
    return (offset * _datePickerState!.widget.width) + (offset * 6);
  }
}
