// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';
import 'package:vt_ui_components/widgets/scrollable_calendar/extra/dimen.dart';

const TextStyle defaultMonthTextStyle = TextStyle(
  color: AppColors.BLACK,
  fontSize: Dimen.monthTextSize,
  fontWeight: FontWeight.w500,
);

const TextStyle defaultDateTextStyle = TextStyle(
  color: AppColors.BLACK,
  fontSize: Dimen.dateTextSize,
  fontWeight: FontWeight.w500,
);

const TextStyle defaultDayTextStyle = TextStyle(
  color: AppColors.BLACK,
  fontSize: Dimen.dayTextSize,
  fontWeight: FontWeight.w500,
);
