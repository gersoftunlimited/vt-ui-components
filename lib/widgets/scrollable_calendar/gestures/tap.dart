/// Signature for a function that detects when a tap is occurred.
///
/// Used by [ScrollableCalendarDatePickerComponentTimeline] for tap detection.
typedef DateSelectionCallback = void Function(DateTime selectedDate);

/// Signature for a function that is called when selected date is changed
///
/// Used by [ScrollableCalendarDatePickerComponentTimeline] for tap detection.
typedef DateChangeListener = void Function(DateTime selectedDate);
