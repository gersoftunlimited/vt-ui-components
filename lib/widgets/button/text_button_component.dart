// 🐦 Flutter imports:
import 'package:flutter/material.dart';

class TextButtonComponent extends StatelessWidget {
  Function onButtonPressed;
  Widget? child;
  bool isFullWidth;
  ButtonStyle? style;
  Alignment textAlignment;

  // final String TAG = "text_button";

  TextButtonComponent(
      {super.key,
      required this.onButtonPressed,
      this.child,
      this.isFullWidth = true,
      this.style,
      this.textAlignment = Alignment.centerLeft});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: TextButton(
          onPressed: () {
            onButtonPressed();
          },
          style: _getButtonStyle(style),
          child: _getChild(child, textAlignment),
        ));
  }

  _getButtonStyle(style) {
    return style;
  }

  _getChild(child, aligmnent) {
    return Container(
        alignment: textAlignment,
        child: child ??
            Text("Aceptar",
                style: const TextStyle(fontWeight: FontWeight.bold)));
  }
}
