// ignore_for_file: must_be_immutable

// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/styles/box/box_styles.dart';
import 'package:vt_ui_components/styles/button/button_styles.dart';
import 'package:vt_ui_components/widgets/scale_animated/scale_animated_component.dart';

// 🌎 Project imports:


class GenericButtonComponent extends StatelessWidget {
  Function onButtonPressed;
  Widget? child;
  bool isFullWidth;
  ButtonStyle? style;
  TextStyle? textStyle;
  bool showButton;

  GenericButtonComponent(
      {super.key,
      required this.onButtonPressed,
      this.child,
      this.isFullWidth = true,
      this.style,
      this.showButton = true});

  @override
  Widget build(BuildContext context) {
    return ScaleAnimatedComponent(
        scale: showButton ? 1.0 : 0.0,
        child: SizedBox(
            width: isFullWidth ? MediaQuery.of(context).size.width : null,
            child: Container(
                decoration:
                    BoxStyles.getBoxRadiusWithAlphaAndShadow(radius: 50),
                child: TextButton(
                  onPressed: () => onButtonPressed(),
                  style: _getButtonStyle(style),
                  child: _getChild(child, textStyle),
                ))));
  }

  _getButtonStyle(style) => style ?? ButtonStyles.getButtonStyle();

  _getChild(child, textStyle) =>
      child ??
      Text("Aceptar",
          style: ButtonStyles.getButtonTextStyle(textStyle: textStyle));
}
