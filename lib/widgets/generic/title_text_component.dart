// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class TitleTextComponent extends StatelessWidget {
  String title;
  String description;
  Widget? rightChild;
  TextStyle? titleTextStyle;
  TextStyle? descriptionTextStyle;
  EdgeInsets? margin;

  TitleTextComponent(
      {super.key,
      required this.title,
      this.description = "",
      this.rightChild,
      this.titleTextStyle,
      this.descriptionTextStyle,
      this.margin});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: margin,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Text(title.toUpperCase(),
                        style: titleTextStyle ?? TextStyles.titleStyle),
                    Container(
                      height: 1,
                      width: 85,
                      margin: const EdgeInsets.only(top: 10),
                      color: AppColors.PRIMARY_COLOR,
                    ),
                    if (description.isNotEmpty)
                      Container(
                          margin: const EdgeInsets.only(top: 10),
                          child: Text(description,
                              style: descriptionTextStyle ??
                                  TextStyles.titleDescription)),
                  ]),
            ),
            if (rightChild != null)
              Container(
                  margin: const EdgeInsets.only(left: 15), child: rightChild!)
          ],
        ));
  }
}
