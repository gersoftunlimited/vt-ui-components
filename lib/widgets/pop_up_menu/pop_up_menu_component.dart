// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

class PopUpMenuComponent extends StatefulWidget {
  final List<PopUpMenuComponentItem> items;
  final Widget child;

  const PopUpMenuComponent({super.key, required this.items, required this.child});

  @override
  State<PopUpMenuComponent> createState() => _PopUpMenuComponentState();
}

class _PopUpMenuComponentState extends State<PopUpMenuComponent> {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
      ),
      onSelected: (index) {
        widget.items.elementAt(index).onPressed();
      },
      itemBuilder: (context) => List<PopupMenuItem>.generate(
          widget.items.length,
          (int index) => PopupMenuItem(
              value: index, child: widget.items.elementAt(index))),
      child: widget.child,
    );
  }
}
