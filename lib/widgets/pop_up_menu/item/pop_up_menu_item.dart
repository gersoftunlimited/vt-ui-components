// 🐦 Flutter imports:
import 'package:flutter/material.dart';

class PopUpMenuComponentItem extends StatelessWidget {
  final Function onPressed;
  final String text;
  TextStyle? textStyle;
  bool showMargins = false;

  PopUpMenuComponentItem(
      {super.key,
      required this.onPressed,
      required this.text,
      this.textStyle,
      this.showMargins = true});

  @override
  Widget build(BuildContext context) => GestureDetector(
      onTap: () {
        Navigator.pop(context);
        onPressed();
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          showMargins ? const SizedBox(width: 5.0) : const SizedBox(),
          Text(text, style: textStyle ?? const TextStyle())
        ],
      ));
}
