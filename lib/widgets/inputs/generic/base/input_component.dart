// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vt_ui_components/export.dart';

class InputComponent extends StatefulWidget {
  String initialValue;
  String hint;
  TextInputType keyboardType;
  Widget? prefixIcon;
  Widget? suffixIcon;
  Function(String value)? onChanged;
  bool isEnabled;
  int? minLines;
  int? maxLines;
  int? maxLength;
  Widget? rightChild;
  bool showHintInLabel;
  bool showHintWhenFocus;
  FocusNode? focusNode;
  Function()? onFieldSubmitted;
  TextInputAction textInputAction;
  TextEditingController? textEditingController;
  bool obscureText;
  Function? onPressed;
  Iterable<String>? autofillHints;
  List<TextInputFormatter>? inputFormatters;

  InputComponent(
      {super.key,
      this.initialValue = "",
      this.hint = "",
      this.prefixIcon,
      this.suffixIcon,
      this.keyboardType = TextInputType.text,
      this.isEnabled = true,
      this.minLines,
      this.maxLines,
      this.maxLength,
      this.rightChild,
      this.inputFormatters,
      this.onChanged,
      this.showHintInLabel = true,
      this.showHintWhenFocus = false,
      this.focusNode,
      this.onFieldSubmitted,
      this.textInputAction = TextInputAction.next,
      this.textEditingController,
      this.onPressed,
      this.autofillHints,
      this.obscureText = false});

  @override
  State<InputComponent> createState() => _InputComponentState();
}

class _InputComponentState extends State<InputComponent> {
  @override
  void initState() {
    super.initState();

    _updateTextValue();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
              child: GestureDetector(
            onTap: widget.onPressed != null && !widget.isEnabled
                ? () {
                    widget.onPressed!();
                  }
                : null,
            child: TextFormField(
              keyboardType: widget.keyboardType,
              autocorrect: false,
              autofillHints: widget.autofillHints,
              controller: widget.textEditingController,
              enabled: widget.isEnabled,
              minLines: widget.minLines ?? 1,
              maxLines: widget.maxLines ?? 1,
              maxLength: widget.maxLength,
              textInputAction: widget.textInputAction,
              obscureText: widget.obscureText,
              inputFormatters: widget.inputFormatters,
              onChanged: (value) {
                if (widget.onChanged != null) {
                  widget.onChanged!(value);
                }
              },
              onFieldSubmitted: (value) {
                if (widget.onChanged != null) {
                  widget.onChanged!(value);
                }
              },
              style: const TextStyle(
                fontSize: 16,
              ),
              decoration: InputStyles.getInputDecoration(
                  hint: widget.hint,
                  prefixIcon: widget.prefixIcon,
                  suffixIcon: widget.suffixIcon,
                  showHintInLabel: widget.showHintInLabel,
                  showHintWhenFocus: widget.showHintWhenFocus),
            ),
          )),
          if (widget.rightChild != null) widget.rightChild!
        ]);
  }

  void _updateTextValue() {
    if (widget.textEditingController != null) {
      widget.textEditingController?.text = widget.initialValue;
    } else {
      widget.textEditingController = TextEditingController();
      widget.textEditingController?.text = widget.initialValue;
    }
  }
}
