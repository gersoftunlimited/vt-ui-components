// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:currency_picker/currency_picker.dart';
import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class CurrencyInputComponent extends StatefulWidget {
  String initialValue;
  String hint;
  String searchHint;
  Widget? prefixIcon;
  Widget? suffixIcon;
  bool isEnabled;
  Function(Currency currency) onChanged;
  Function? onPressed;


  CurrencyInputComponent({
    super.key,
    required this.onChanged,
    this.initialValue = "",
    required this.hint,
    required this.searchHint,
    this.prefixIcon,
    this.suffixIcon,
    this.isEnabled = true,
    this.onPressed,
  });

  @override
  State<CurrencyInputComponent> createState() => _CurrencyInputComponentState();
}

class _CurrencyInputComponentState extends State<CurrencyInputComponent> {
  final String TAG = "currency_input";

  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (widget.isEnabled) {
          showCurrencyPicker(
            context: context,
            showFlag: true,
            theme: CurrencyPickerThemeData(
              flagSize: 25,
              backgroundColor: AppColors.WHITE,
              titleTextStyle: const TextStyle(fontSize: 17),
              subtitleTextStyle:
                  TextStyle(fontSize: 15, color: Theme.of(context).hintColor),
              bottomSheetHeight: MediaQuery.of(context).size.height / 1.5,
            ),
            showCurrencyName: true,
            showCurrencyCode: true,
            showSearchField: false,
            searchHint: widget.searchHint,
            onSelect: (Currency currency) {
              controller.text = currency.code;
              widget.onChanged(currency);
            },
          );
        }
      },
      child: InputComponent(
        onChanged: (String value) {},
        isEnabled: false,
        initialValue: widget.initialValue,
        textEditingController: controller,
        hint: widget.hint,
        suffixIcon: const Icon(
          Icons.arrow_forward_ios_rounded,
          size: 14,
        ),
      ),
    );
  }
}
