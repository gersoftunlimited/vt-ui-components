// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class DropdownInputComponent extends StatefulWidget {
  Widget? prefixIcon;
  String hint;
  bool initiallyExpanded;
  List<DropDownValueModel> dropDownList;
  int dropDownItemCount;
  bool enableSearch;
  bool clearOption;
  String searchHintText;
  bool isEnabled;
  String initialValue;
  Function(DropDownValueModel item) onItemChanged;

  DropdownInputComponent(
      {super.key,
      required this.dropDownList,
      required this.onItemChanged,
      this.hint = "",
      this.prefixIcon,
      this.dropDownItemCount = 6,
      this.enableSearch = false,
      this.clearOption = false,
      this.searchHintText = "Escribe algo",
      this.isEnabled = true,
      this.initiallyExpanded = false,
      this.initialValue = ""});

  @override
  State<DropdownInputComponent> createState() => _DropdownInputComponentState();
}

class _DropdownInputComponentState extends State<DropdownInputComponent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: _getInput(),
    );
  }

  _getInput() => Theme(
      data: Theme.of(context).copyWith(
        dividerColor: Colors.transparent,
      ),
      child: _singleDropdownInput());

  _singleDropdownInput() => DropDownTextField(
        initialValue: widget.initialValue,
        clearOption: widget.clearOption,
        enableSearch: widget.enableSearch,
        isEnabled: widget.isEnabled,
        clearIconProperty: IconProperty(color: AppColors.DROPDOWN_ARROW_COLOR),
        dropDownIconProperty: IconProperty(
            color: AppColors.DROPDOWN_ARROW_COLOR,
            icon: Icons.keyboard_arrow_down_rounded),
        textFieldDecoration: InputStyles.getInputDecoration(
            hint: widget.hint, prefixIcon: widget.prefixIcon),
        dropDownItemCount: widget.dropDownItemCount,
        searchDecoration: InputStyles.getInputDecoration(
            prefixIcon: widget.prefixIcon,
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 0, bottom: 0)),
        dropDownList: widget.dropDownList
            .map((e) => DropDownValueModel(
                name: e.name, value: e.value, toolTipMsg: e.toolTipMsg))
            .toList(),
        onChanged: (item) {
          /* If selects item returns item DropDownValueModel, if pressed clear icon returns empty string */
          widget.onItemChanged(item);
        },
      );
}
