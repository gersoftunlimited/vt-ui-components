// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

// 📦 Package imports:
import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class DropdownMultiSelectInputComponent extends StatefulWidget {
  Widget? prefixIcon;
  String hint;
  bool initiallyExpanded;
  List<DropDownValueModel> dropDownList;
  int dropDownItemCount;
  bool isEnabled;
  List<String> initialValue;
  Function(List<DropDownValueModel> items) onItemChanged;

  DropdownMultiSelectInputComponent(
      {super.key,
      required this.dropDownList,
      required this.onItemChanged,
      this.hint = "",
      this.prefixIcon,
      this.dropDownItemCount = 6,
      this.isEnabled = true,
      this.initiallyExpanded = false,
      this.initialValue = const []});

  @override
  State<DropdownMultiSelectInputComponent> createState() =>
      _DropdownMultiSelectInputComponentState();
}

class _DropdownMultiSelectInputComponentState
    extends State<DropdownMultiSelectInputComponent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: _getInput(),
    );
  }

  _getInput() => Theme(
      data: Theme.of(context).copyWith(
        dividerColor: Colors.transparent,
      ),
      child: _multipleDropdownInput());

  _multipleDropdownInput() => DropDownTextField.multiSelection(
        initialValue: widget.initialValue,
        isEnabled: widget.isEnabled,
        clearIconProperty: IconProperty(color: AppColors.DROPDOWN_ARROW_COLOR),
        dropDownIconProperty: IconProperty(
            color: AppColors.DROPDOWN_ARROW_COLOR,
            icon: Icons.keyboard_arrow_down_rounded),
        textFieldDecoration: InputStyles.getInputDecoration(
            hint: widget.hint, prefixIcon: widget.prefixIcon),
        dropDownItemCount: widget.dropDownItemCount,
        dropDownList: widget.dropDownList
            .map((e) => DropDownValueModel(
                name: e.name, value: e.value, toolTipMsg: e.toolTipMsg))
            .toList(),
        checkBoxProperty: CheckBoxProperty(activeColor: AppColors.GREEN),
        submitButtonColor: AppColors.PRIMARY_COLOR,
        submitButtonText: "CONFIRMAR",
        clearOption: false,
        displayCompleteItem: true,
        listTextStyle: const TextStyle(
          color: AppColors.BLACK,
          fontWeight: FontWeight.bold,
        ),
        submitButtonTextStyle: const TextStyle(
          color: AppColors.WHITE,
          fontWeight: FontWeight.bold,
        ),
        onChanged: (item) {
          if (item.isEmpty) {
            item = [].cast<DropDownValueModel>();
          }
          /* If selects item returns item DropDownValueModel, if pressed clear icon returns empty string */
          widget.onItemChanged(item);
        },
      );
}
