// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/widgets/dialog/time/time_picker_dialog.dart';

class TimePickerInputComponent extends StatefulWidget {
  Widget? child;
  Function onTimeChanged;
  DateTime? initialDateTime;
  bool use24hFormat;
  TextStyle? cancelButtonTextStyle;
  TextStyle? acceptButtonTextStyle;

  TimePickerInputComponent(
      {super.key,
      this.child,
      required this.onTimeChanged,
      this.initialDateTime,
      this.use24hFormat = true,
      this.cancelButtonTextStyle,
      this.acceptButtonTextStyle});

  @override
  State<StatefulWidget> createState() {
    return _TimePickerInputComponent();
  }
}

class _TimePickerInputComponent extends State<TimePickerInputComponent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          TimePickerComponentDialog.show(context,
              onTimeChanged: widget.onTimeChanged,
              initialDateTime: widget.initialDateTime,
              use24hFormat: widget.use24hFormat,
              cancelButtonTextStyle: widget.cancelButtonTextStyle,
              acceptButtonTextStyle: widget.acceptButtonTextStyle);
        },
        child: widget.child);
  }
}
