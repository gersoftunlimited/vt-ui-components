// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';

class DNIInputComponent extends StatefulWidget {
  Widget? prefixIcon;
  String hint;
  bool initiallyExpanded;
  List<RadioButtonGroupModel> labels;
  String documentType;
  String initialValue;
  Function(RadioButtonGroupModel selected) onDocumentSelected;
  Function(String value) onValueChanged;
  TextEditingController? controller;
  bool isEnabled;
  String identificativeDocumentLiteral;
  var documentTypes;

  DNIInputComponent(
      {super.key,
      this.hint = "",
      this.prefixIcon,
      this.labels = const [],
      this.documentType = "",
      this.initialValue = "",
      this.controller,
      this.isEnabled = true,
      this.initiallyExpanded = false,
      required this.onDocumentSelected,
      required this.onValueChanged,
      required this.identificativeDocumentLiteral,
      required this.documentTypes});

  @override
  State<DNIInputComponent> createState() => _DNIInputComponentState();
}

class _DNIInputComponentState extends State<DNIInputComponent> {
  final String TAG = "dni_input";

  String documentId = "";

  @override
  void initState() {
    super.initState();

    if (widget.labels.isNotEmpty && widget.documentType.isEmpty) {
      widget.documentType = widget.labels.first.label;
    }

    documentId = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {},
        child: Wrap(
          runSpacing: 10,
          children: [_getInput()],
        ));
  }

  _getInput() => Theme(
      data: Theme.of(context).copyWith(
        dividerColor: Colors.transparent,
      ),
      child: ExpansionTile(
          collapsedIconColor: AppColors.DROPDOWN_ARROW_COLOR,
          iconColor: AppColors.DROPDOWN_ARROW_COLOR,
          initiallyExpanded: widget.initialValue.isNotEmpty,
          onExpansionChanged: (value) {},
          tilePadding: EdgeInsets.zero,
          maintainState: true,
          title: TextFormField(
            enabled: false,
            style: const TextStyle(
              fontSize: 16,
            ),
            decoration: InputStyles.getInputDecoration(
                hint: widget.hint, prefixIcon: widget.prefixIcon),
          ),
          children: [_getDocumentTypesComponent(), _getDocumentInput()]));

  _getDocumentTypesComponent() {
    return Container(
      margin: const EdgeInsets.only(top: 5, bottom: 5, left: 20, right: 20),
      child: RadioButtonGroupComponent(
        isEnabled: widget.isEnabled,
        options: widget.documentTypes
            .map((document) =>
                RadioButtonGroupModel(id: document.id, label: document.type))
            .toList()
            .cast<RadioButtonGroupModel>(),
        picked: widget.documentType,
        onItemSelected: (RadioButtonGroupModel selected) {
          setState(() {
            widget.documentType = selected.label;
            widget.onDocumentSelected(selected);
          });
        },
      ),
    );
  }

  _getDocumentInput() => Container(
      margin: const EdgeInsets.only(top: 10, left: 20, right: 20, bottom: 10),
      child: InputComponent(
        isEnabled: widget.isEnabled,
        onChanged: (String value) {
          documentId = value;
          widget.onValueChanged(documentId);
        },
        initialValue: widget.initialValue,
        hint: _getDocumentInputHint(),
      ));

  _getDocumentInputHint() {
    if (widget.documentType == "CIF") {
      return "Ej: B12345678";
    } else if (widget.documentType == "NIF") {
      return "Ej: 12345678X";
    } else if (widget.documentType == "NIE") {
      return "Ej: X12345678A";
    } else if (widget.documentType == "DNI") {
      return "Ej: 12345678A";
    }

    return widget.identificativeDocumentLiteral;
  }
}
