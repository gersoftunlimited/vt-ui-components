// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:utils/export.dart';

// 🌎 Project imports:
import 'package:vt_ui_components/export.dart';

class DatePickerInputComponent extends StatefulWidget {
  Widget? child;
  Function onDateTimeChanged;
  DateTime? initialDateTime;
  DateTime? minimumDate;
  DateTime? maximumDate;
  int? maximumYear;
  int? minimumYear;
  TextStyle? cancelButtonTextStyle;
  TextStyle? acceptButtonTextStyle;
  CupertinoDatePickerMode mode;
  bool hasLimits;
  bool use24hFormat;
  String inputHint;
  String locale;

  DatePickerInputComponent(
      {super.key,
      this.child,
      required this.onDateTimeChanged,
      this.initialDateTime,
      this.minimumDate,
      this.maximumDate,
      this.maximumYear,
      this.minimumYear,
      this.mode = CupertinoDatePickerMode.date,
      this.hasLimits = true,
      this.inputHint = "",
      this.use24hFormat = true,
      required this.locale});

  @override
  State<StatefulWidget> createState() {
    return _DatePickerInputComponent();
  }
}

class _DatePickerInputComponent extends State<DatePickerInputComponent> {
  TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _getInitalDate();

    return GestureDetector(
      onTap: () {
        DatePickerComponentDialog.show(context, onDateTimeChanged: (date) {
          textEditingController.text = DateTimeUtils.parseDateTimeToDay(date,
              LanguageUtils.getLocale(),
              showHours: widget.mode == CupertinoDatePickerMode.dateAndTime);
          widget.onDateTimeChanged(date);
          widget.initialDateTime = date;
          setState(() {});
        },
            initialDateTime: widget.initialDateTime,
            maximumDate: widget.maximumDate ??
                DateTime.now().add(const Duration(days: 365)),
            minimumDate: widget.minimumDate,
            maximumYear: widget.maximumYear,
            minimumYear: widget.minimumYear,
            cancelButtonTextStyle: widget.cancelButtonTextStyle,
            acceptButtonTextStyle: widget.acceptButtonTextStyle,
            mode: widget.mode,
            hasLimits: widget.hasLimits,
            use24hFormat: widget.use24hFormat,
            locale: widget.locale,
            inputHint: widget.inputHint);
      },
      child: InputComponent(
        onChanged: (value) {},
        hint: widget.inputHint,
        initialValue: textEditingController.text,
        textEditingController: textEditingController,
        suffixIcon: const Icon(Icons.calendar_today_rounded),
        isEnabled: false,
      ),
    );
  }

  void _getInitalDate() {
    if (widget.initialDateTime != null) {
      textEditingController.text = DateTimeUtils.parseDateTimeToDay(
          widget.initialDateTime, LanguageUtils.getLocale(),
          showHours: widget.mode == CupertinoDatePickerMode.dateAndTime);
    } else {
      textEditingController.text = "";
    }
  }
}
