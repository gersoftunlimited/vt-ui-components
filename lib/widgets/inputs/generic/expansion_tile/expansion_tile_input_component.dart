// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class ExpansionTileInputComponent extends StatefulWidget {
  Widget? prefixIcon;
  String hint;
  String value;
  bool initiallyExpanded;
  List<Widget> children;
  TextEditingController? controller;

  ExpansionTileInputComponent(
      {super.key,
      required this.children,
      this.hint = "",
      this.prefixIcon,
      this.value = "",
      this.controller,
      this.initiallyExpanded = false});

  @override
  State<ExpansionTileInputComponent> createState() =>
      _ExpansionTileInputComponentState();
}

class _ExpansionTileInputComponentState
    extends State<ExpansionTileInputComponent> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {},
        child: Wrap(
          runSpacing: 10,
          children: [_getInput()],
        ));
  }

  _getInput() => Theme(
      data: Theme.of(context).copyWith(
        dividerColor: Colors.transparent,
      ),
      child: ExpansionTile(
          collapsedIconColor: AppColors.DROPDOWN_ARROW_COLOR,
          iconColor: AppColors.DROPDOWN_ARROW_COLOR,
          initiallyExpanded: widget.initiallyExpanded,
          onExpansionChanged: (value) {},
          tilePadding: EdgeInsets.zero,
          maintainState: true,
          title: InputComponent(
            hint: widget.hint,
            textEditingController: widget.controller,
            isEnabled: false,
            onChanged: (value) {},
            initialValue: widget.value,
          ),
          childrenPadding: const EdgeInsets.only(top: 10),
          children: widget.children));
}
