// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class PasswordInputComponent extends StatefulWidget {
  String initialValue;
  String hint;
  Widget? prefixIcon;
  Widget? suffixIcon;
  Function(String value) onChanged;
  bool isEnabled;
  TextInputAction textInputAction;
  Function(String value)? onFieldSubmitted;
  Iterable<String>? autofillHints;

  PasswordInputComponent(
      {super.key,
      this.initialValue = "",
      this.hint = "",
      this.prefixIcon,
      this.suffixIcon,
      this.isEnabled = true,
      required this.onChanged,
      this.textInputAction = TextInputAction.next,
      this.autofillHints,
      this.onFieldSubmitted});

  @override
  State<PasswordInputComponent> createState() => _PasswordInputComponentState();
}

class _PasswordInputComponentState extends State<PasswordInputComponent> {
  bool hiddePassword = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.text,
      obscureText: hiddePassword,
      autocorrect: false,
      enabled: widget.isEnabled,
      autofillHints: widget.autofillHints,
      textInputAction: widget.textInputAction,
      onChanged: (value) => {widget.onChanged(value)},
      onFieldSubmitted: (value) {
        widget.onFieldSubmitted!(value);
      },
      style: const TextStyle(
        fontSize: 16,
      ),
      decoration: InputStyles.getInputDecoration(
          hint: widget.hint,
          prefixIcon: widget.prefixIcon,
          suffixIcon: _getSuffixIcon()),
    );
  }

  _getSuffixIcon() => GestureDetector(
      onTap: () {
        hiddePassword = !hiddePassword;
        setState(() {});
      },
      child: Icon(
          hiddePassword
              ? Icons.visibility_outlined
              : Icons.visibility_off_outlined,
          semanticLabel: hiddePassword
              ? "Icono ojo mostrar contraseña"
              : "Icono ojo tachado no mostrar contraseña"));
}
