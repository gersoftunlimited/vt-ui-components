// 🐦 Flutter imports:
// ignore_for_file: must_be_immutable
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

// 📦 Package imports:
import 'package:utils/export.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class PhoneInputComponent extends StatefulWidget {
  String initialValue;
  String hint;
  Widget? prefixIcon;
  Widget? suffixIcon;
  Function(PhoneNumber? phoneNumber, bool isValid) onChanged;
  bool isEnabled;
  TextInputAction textInputAction;
  List<String>? countries;
  Function? onPressed;

  PhoneInputComponent(
      {super.key,
      this.initialValue = "",
      this.hint = "",
      this.prefixIcon,
      this.suffixIcon,
      this.isEnabled = true,
      required this.onChanged,
      this.onPressed,
      this.countries,
      this.textInputAction = TextInputAction.next});

  @override
  State<PhoneInputComponent> createState() => _PhoneInputComponentState();
}

class _PhoneInputComponentState extends State<PhoneInputComponent> {
  TextEditingController controller = TextEditingController();

  PhoneNumber? phoneNumber;

  @override
  void initState() {
    super.initState();
    fetchInitialPhoneNumber();
  }

  fetchInitialPhoneNumber() {
    if (widget.initialValue.isEmpty) {
      phoneNumber = defaultEmptyPhoneNumber();
      return;
    }

    PhoneNumber.getRegionInfoFromPhoneNumber(widget.initialValue, "es")
        .then((value) {
      phoneNumber = (value.isoCode == null || value.dialCode == null)
          ? defaultEmptyPhoneNumber()
          : value;

      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          if (widget.onPressed != null && !widget.isEnabled) {
            widget.onPressed!();
          }
        },
        child: InternationalPhoneNumberInput(
          textFieldController: controller,
          onInputChanged: (PhoneNumber number) {
            phoneNumber = number;
          },
          onInputValidated: (bool isValid) {
            widget.onChanged(phoneNumber, isValid);
          },
          selectorConfig: const SelectorConfig(
            setSelectorButtonAsPrefixIcon: true,
            leadingPadding: 15,
            useEmoji: true,
            trailingSpace: false,
            selectorType: PhoneInputSelectorType.DIALOG,
          ),
          ignoreBlank: false,
          isEnabled: widget.isEnabled,
          searchBoxDecoration:
              InputStyles.getInputDecoration(hint: "Buscar país o código"),
          autoValidateMode: AutovalidateMode.disabled,
          selectorTextStyle: const TextStyle(color: Colors.black),
          formatInput: true,
          locale: LanguageUtils.getLocale(),
          countries: widget.countries,
          initialValue: phoneNumber ?? defaultEmptyPhoneNumber(),
          keyboardType: const TextInputType.numberWithOptions(
              signed: true, decimal: true),
          inputDecoration: InputStyles.getInputDecoration(
              hint: widget.hint, suffixIcon: widget.suffixIcon),
        ));
  }

  defaultEmptyPhoneNumber() {
    return PhoneNumber(isoCode: "ES");
  }
}
