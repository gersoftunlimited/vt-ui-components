import 'package:flutter/material.dart';
import 'package:vt_ui_components/app_colors/app_colors.dart';

class InfoMessageComponent extends StatefulWidget {
  Widget? icon;
  String title;
  Duration? duration;
  Color textColor;
  Color backgroundColor;
  Function? onPressed;
  bool isAnimated;
  TextStyle textStyle;
  EdgeInsets padding;
  TextAlign textAlign;

  InfoMessageComponent(
      {super.key,
      this.icon,
      required this.title,
      this.duration,
      this.backgroundColor = AppColors.SOFT_GREY,
      this.onPressed,
      this.isAnimated = true,
      this.textAlign = TextAlign.center,
      this.padding =
          const EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
      this.textStyle =
          const TextStyle(fontWeight: FontWeight.bold, color: AppColors.BLACK),
      this.textColor = AppColors.BLACK});

  @override
  State<StatefulWidget> createState() => _InfoMessageComponent();
}

class _InfoMessageComponent extends State<InfoMessageComponent>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<Offset> offset;
  bool hide = false;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: widget.isAnimated ? 300 : 0));

    offset = Tween<Offset>(
            begin: const Offset(0.0, -2.0), end: const Offset(0.0, 0.0))
        .animate(controller);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.forward().then((value) {
        if (widget.duration != null) {
          Future.delayed(widget.duration!).then((value) {
            controller.reverse().then((value) => setState(() {
                  hide = true;
                }));
          });
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return hide
        ? Container(
            height: 0.1,
          )
        : GestureDetector(
            onTap: () {
              if (widget.onPressed != null) {
                widget.onPressed!();
              }
            },
            child: SlideTransition(
                position: offset,
                child: Container(
                  padding: widget.padding,
                  color: widget.backgroundColor,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if (widget.icon != null)
                        Container(
                            margin: const EdgeInsets.only(right: 5),
                            child: widget.icon!),
                      Expanded(
                        child: Container(
                            padding: const EdgeInsets.only(bottom: 5, top: 5),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              widget.title,
                              textAlign: widget.textAlign,
                              style: widget.textStyle,
                            )),
                      )
                    ],
                  ),
                )),
          );
  }
}
