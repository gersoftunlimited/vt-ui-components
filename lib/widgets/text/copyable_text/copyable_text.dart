import 'package:flutter/material.dart';
import 'package:utils/export.dart';

class CopyableText extends StatelessWidget {
  String text;
  bool enableCopy;
  TextStyle? textStyle;
  Function()? onCopySuccess;
  CopyableText(
      {super.key,
      this.text = "",
      this.enableCopy = true,
      this.textStyle,
      this.onCopySuccess});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (enableCopy && onCopySuccess != null) {
          ClipboardUtils.copyToClipboard(text: text).then((value) {
            onCopySuccess!();
          });
        }
      },
      child: Text(
        text,
        style: textStyle ??
            const TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
      ),
    );
  }
}
