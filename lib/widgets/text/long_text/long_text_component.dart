import 'package:flutter/material.dart';

class LongTextComponent extends StatefulWidget {
  BuildContext context;
  String text;
  EdgeInsets margins;

  LongTextComponent(
      {required this.context,
      required this.text,
      this.margins = const EdgeInsets.only(left: 20, right: 20, top: 20)});

  @override
  State<LongTextComponent> createState() => _LongTextComponentState();
}

class _LongTextComponentState extends State<LongTextComponent> {
  double? descriptionHeight = 100;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: widget.margins,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              child: Text(widget.text,
                  maxLines: descriptionHeight == 100 ? 5 : null,
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 14, height: 1.5))),
          _showMoreLessIcon()
        ],
      ),
    );
  }

  _showMoreLessIcon() => (widget.text.length >= 300)
      ? GestureDetector(
          onTap: () {
            setState(() {
              descriptionHeight == 100
                  ? descriptionHeight = null
                  : descriptionHeight = 100;
            });
          },
          child: Container(
            padding: const EdgeInsets.only(top: 10),
            child: descriptionHeight != 100
                ? Semantics(
                    label: "Ver menos",
                    child: Wrap(
                      children: const [
                        Icon(Icons.visibility_off_outlined),
                        Icon(Icons.remove)
                      ],
                    ))
                : Semantics(
                    label: "Ver más",
                    child: Wrap(
                      children: const [
                        Icon(Icons.visibility_outlined),
                        Icon(Icons.add)
                      ],
                    )),
          ),
        )
      : Container();
}
