// 🐦 Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

class TimePickerComponentDialog {
  static DateTime today = DateTime.now();
  static DateTime? _dateSelected;

  static show(BuildContext context,
      {Color? backgroundColor = AppColors.WHITE,
      bool isDismissible = true,
      required Function onTimeChanged,
      TextStyle? cancelButtonTextStyle,
      TextStyle? acceptButtonTextStyle,
      DateTime? initialDateTime,
      bool use24hFormat = true}) {
    _dateSelected = _getInitialDate(initialDateTime);

    return showDialog<void>(
      context: context,
      barrierDismissible: isDismissible,
      useSafeArea: true,
      builder: (BuildContext buildContext) {
        return AlertDialog(
            contentPadding: EdgeInsets.zero,
            backgroundColor: Colors.transparent,
            elevation: 0,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            content: Container(
              constraints: const BoxConstraints(maxHeight: 350),
              padding: _getTopMargin(),
              decoration: BoxDecoration(
                color: backgroundColor,
                borderRadius: const BorderRadius.all(Radius.circular(20.0)),
              ),
              child: Column(
                children: [
                  _confirmButtons(
                      context: context,
                      onTimeChanged: onTimeChanged,
                      cancelButtonTextStyle: cancelButtonTextStyle,
                      acceptButtonTextStyle: acceptButtonTextStyle),
                  _getDatePickerWidget(
                      context: context,
                      initialDateTime: initialDateTime,
                      use24hFormat: use24hFormat)
                ],
              ),
            ));
      },
    );
  }

  static _getTopMargin() {
    return const EdgeInsets.only(top: 10, right: 20, left: 20, bottom: 20);
  }

  static _confirmButtons(
      {required context,
      required Function onTimeChanged,
      TextStyle? cancelButtonTextStyle,
      TextStyle? acceptButtonTextStyle}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: [
        TextButton(
          onPressed: () {
            _closeDialog(context);
          },
          child: Text(
            "Cancelar",
            style:
                cancelButtonTextStyle ?? TextStyles.dateCancelButtonTextStyle,
          ),
        ),
        TextButton(
          onPressed: () {
            _closeDialog(context);
            onTimeChanged(_dateSelected);
          },
          child: Text(
            "Aceptar",
            style:
                acceptButtonTextStyle ?? TextStyles.dateAcceptButtonTextStyle,
          ),
        ),
      ],
    );
  }

  static _closeDialog(context) {
    Navigator.pop(context);
  }

  static _getDatePickerWidget(
      {required BuildContext context,
      DateTime? initialDateTime,
      bool use24hFormat = true}) {
    return Expanded(
        child: Container(
      width: MediaQuery.of(context).size.width,
      constraints: const BoxConstraints(maxWidth: 500),
      child: CupertinoDatePicker(
        mode: CupertinoDatePickerMode.time,
        use24hFormat: use24hFormat,
        initialDateTime: _getInitialDate(initialDateTime),
        onDateTimeChanged: (DateTime newDateTime) {
          _dateSelected = newDateTime;
        },
      ),
    ));
  }

  static _getInitialDate(DateTime? initialDateTime) {
    return initialDateTime ?? today;
  }
}
