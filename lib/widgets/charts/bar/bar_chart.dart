import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

import '../../app_bar/app_bar_button_component.dart';

class BarChartComponent extends StatefulWidget {
  BarChartComponent(
      {required this.onLeftIconPressed, required this.onRightIconPressed});
  final Color leftBarColor = AppColors.GREEN;
  final Color rightBarColor = AppColors.RED;
  final Color avgColor = AppColors.APP_BAR_BACKGROUND_COLOR;

  Function onLeftIconPressed;
  Function onRightIconPressed;

  @override
  State<StatefulWidget> createState() => BarChartComponentState();
}

class BarChartComponentState extends State<BarChartComponent> {
  final double width = 5;

  late List<BarChartGroupData> rawBarGroups;
  late List<BarChartGroupData> showingBarGroups;

  int touchedGroupIndex = -1;

  @override
  void initState() {
    super.initState();
    final barGroup1 = makeGroupData(0, 5, 12);
    final barGroup2 = makeGroupData(1, 16, 12);
    final barGroup3 = makeGroupData(2, 18, 5);
    final barGroup4 = makeGroupData(3, 20, 16);
    final barGroup5 = makeGroupData(3, 20, 2);

    final items = [barGroup1, barGroup2, barGroup3, barGroup4, barGroup5];

    rawBarGroups = items;

    showingBarGroups = rawBarGroups;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        VTUIAppBarButtonComponent(
            icon: Icon(Icons.arrow_back_ios_rounded),
            onPressed: () {
              widget.onLeftIconPressed();
            }),
        Expanded(
            child: BarChart(
          BarChartData(
            maxY: 20,
            barTouchData: BarTouchData(
              touchTooltipData: BarTouchTooltipData(
                tooltipBgColor: Colors.black,
                getTooltipItem: (a, b, c, d) {
                  ToastUtils.displayToast("Hola");

                  return BarTooltipItem(
                      c.toY.toString(),
                      const TextStyle(
                          fontWeight: FontWeight.bold, color: AppColors.WHITE));
                },
              ),
              touchCallback: (FlTouchEvent event, response) {},
            ),
            titlesData: FlTitlesData(
              show: true,
              rightTitles: AxisTitles(
                sideTitles: SideTitles(showTitles: false),
              ),
              topTitles: AxisTitles(
                sideTitles: SideTitles(showTitles: false),
              ),
              bottomTitles: AxisTitles(
                sideTitles: SideTitles(
                  showTitles: true,
                  getTitlesWidget: bottomTitles,
                  reservedSize: 42,
                ),
              ),
              leftTitles: AxisTitles(
                sideTitles: SideTitles(
                  showTitles: false,
                  reservedSize: 28,
                  interval: 1,
                  getTitlesWidget: leftTitles,
                ),
              ),
            ),
            borderData: FlBorderData(
              show: false,
            ),
            barGroups: showingBarGroups,
            gridData: FlGridData(show: false),
          ),
        )),
        VTUIAppBarButtonComponent(
            icon: const Icon(Icons.arrow_forward_ios_outlined),
            onPressed: () {
              widget.onRightIconPressed();
            }),
      ],
    );
  }

  Widget leftTitles(double value, TitleMeta meta) {
    const style = TextStyle(
      color: Color(0xff7589a2),
      fontWeight: FontWeight.bold,
      fontSize: 14,
    );
    String text;
    if (value == 0) {
      text = '1K';
    } else if (value == 10) {
      text = '5K';
    } else if (value == 19) {
      text = '10K';
    } else {
      return Container();
    }
    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 0,
      child: Text(text, style: style),
    );
  }

  Widget bottomTitles(double value, TitleMeta meta) {
    final titles = <String>[
      'Oct 22',
      'Nov 22',
      'Dic 22',
      'Ene 23',
      'Feb 23',
    ];

    final Widget text = Text(
      titles[value.toInt()],
      style: const TextStyle(
        color: Color(0xff7589a2),
        fontWeight: FontWeight.bold,
        fontSize: 14,
      ),
    );

    return SideTitleWidget(
      axisSide: meta.axisSide,
      space: 16, //margin top
      child: text,
    );
  }

  BarChartGroupData makeGroupData(int x, double y1, double y2) {
    return BarChartGroupData(
      barsSpace: 4,
      x: x,
      barRods: [
        BarChartRodData(
          toY: y1,
          color: widget.leftBarColor,
          width: width,
        ),
        BarChartRodData(
          toY: y2,
          color: widget.rightBarColor,
          width: width,
        ),
      ],
    );
  }
}
