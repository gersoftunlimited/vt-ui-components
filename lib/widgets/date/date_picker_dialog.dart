// 🐦 Flutter imports:
import 'package:utils/export.dart';
import 'package:vt_ui_components/export.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class DatePickerComponentDialog {
  static DateTime today = DateTime.now();
  static DateTime? _dateSelected;

  static show(BuildContext context,
      {Color? backgroundColor = AppColors.WHITE,
      bool isDismissible = true,
      String inputHint = "",
      required Function onDateTimeChanged,
      DateTime? initialDateTime,
      DateTime? minimumDate,
      DateTime? maximumDate,
      int? maximumYear,
      int? minimumYear,
      TextStyle? cancelButtonTextStyle,
      CupertinoDatePickerMode? mode,
      bool hasLimits = true,
      bool use24hFormat = true,
      TextStyle? acceptButtonTextStyle,
      required String locale}) {
    _dateSelected = _getInitialDate(initialDateTime);

    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: AppColors.WHITE,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.0),
      ),
      builder: (context) {
        return StatefulBuilder(builder: (context, setState) {
          return Container(
            constraints: const BoxConstraints(maxHeight: 500),
            padding: _getTopMargin(),
            child: Column(
              children: [
                _dragIcon(),
                _currentDateText(inputHint: inputHint, mode: mode, locale: locale),
                _getDatePickerWidget(
                    context: context,
                    initialDateTime: initialDateTime,
                    minimumDate: minimumDate,
                    maximumDate: maximumDate,
                    maximumYear: maximumYear,
                    minimumYear: minimumYear,
                    mode: mode,
                    hasLimits: hasLimits,
                    use24hFormat: use24hFormat,
                    setState: setState),
                _confirmButtons(context, onDateTimeChanged)
              ],
            ),
          );
        });
      },
    );
  }

  static Column _confirmButtons(
      BuildContext context, Function onDateTimeChanged) {
    return Column(
      children: [
        Column(
          children: [
            _acceptButton(context, onDateTimeChanged),
          ],
        ),
      ],
    );
  }

  static Container _acceptButton(
      BuildContext context, Function onDateTimeChanged) {
    return Container(
        margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
        child: GenericButtonComponent(onButtonPressed: () {
          _closeDialog(context);
          onDateTimeChanged(_dateSelected);
        }));
  }

  static _dragIcon() => const Icon(
        Icons.drag_handle_rounded,
        color: AppColors.GREY,
      );

  static _currentDateText(
      {String inputHint = "", CupertinoDatePickerMode? mode, required String locale}) {
    return Column(
      children: [
        if (inputHint.isNotEmpty)
          Container(
              margin: const EdgeInsets.only(top: 10),
              child: Text(
                inputHint,
                style: const TextStyle(fontWeight: FontWeight.bold),
              )),
        Container(
          margin: const EdgeInsets.only(top: 10),
          child: Text(
            DateTimeUtils.parseDateTimeToDay(_dateSelected,
                locale,
                showHours: mode == CupertinoDatePickerMode.dateAndTime),
          ),
        ),
      ],
    );
  }

  static _getTopMargin() {
    return const EdgeInsets.only(top: 10, right: 20, left: 20, bottom: 20);
  }

  static _closeDialog(context) {
    Navigator.pop(context);
  }

  static _getDatePickerWidget(
      {required BuildContext context,
      DateTime? initialDateTime,
      DateTime? minimumDate,
      DateTime? maximumDate,
      int? maximumYear,
      int? minimumYear,
      CupertinoDatePickerMode? mode,
      bool hasLimits = true,
      bool use24hFormat = true,
      required Function setState}) {
    return Expanded(
        child: Container(
            width: MediaQuery.of(context).size.width,
            constraints: const BoxConstraints(maxWidth: 500),
            child: SizedBox(
              child: CupertinoDatePicker(
                mode: mode!,
                initialDateTime: _getInitialDate(initialDateTime),
                minimumDate: hasLimits ? _getMinimumDate(minimumDate) : null,
                maximumDate: hasLimits ? _getMaximumDate(maximumDate) : null,
                maximumYear: hasLimits ? _getMaximumYear(maximumYear) : null,
                minimumYear: _getMinimumYear(minimumYear),
                use24hFormat: use24hFormat,
                dateOrder: DatePickerDateOrder.dmy,
                onDateTimeChanged: (DateTime newDateTime) {
                  _dateSelected = newDateTime;
                  setState(() {});
                },
              ),
            )));
  }

  static _getMaximumYear(int? maximumYear) {
    return maximumYear ?? today.year;
  }

  static _getMinimumYear(int? minimumYear) {
    return minimumYear ?? today.year - 95;
  }

  static _getInitialDate(DateTime? initialDateTime) {
    return initialDateTime ?? today;
  }

  static _getMinimumDate(DateTime? minimumDate) {
    return minimumDate ?? DateTime(today.year - 95, today.month, today.day);
  }

  static _getMaximumDate(DateTime? minimumDate) {
    return minimumDate ?? today;
  }
}
