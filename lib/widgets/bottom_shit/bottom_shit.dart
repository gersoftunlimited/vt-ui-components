/// Named bottom shit, even though, we know that it's named bottom sheet
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

class ModalBottomShit {
  static Future<void> show(BuildContext context, List<Widget> children) async {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      enableDrag: true,
      isDismissible: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(15),
        ),
      ),
      builder: (context) => StatefulBuilder(
          builder: (BuildContext contextParam, setState) => Container(
                decoration: const BoxDecoration(
                    color: AppColors.WHITE,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        topRight: Radius.circular(20.0))),
                child: Container(
                  padding: MarginStyles.getPageMargins(),
                  constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height) *
                      0.75,
                  child: ListView(shrinkWrap: true, children: children),
                ),
              )),
    );
  }
}
