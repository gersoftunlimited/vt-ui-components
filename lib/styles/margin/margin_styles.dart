// 🐦 Flutter imports:
import 'package:flutter/material.dart';

class MarginStyles {
  static getPageMargins() {
    return const EdgeInsets.only(left: 20, right: 20, top: 20);
  }

  static getPageMarginsWithBottom() {
    return const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 30);
  }

  static getLoadingMargins() {
    return const EdgeInsets.all(20);
  }

  static getCardMargins() {
    return const EdgeInsets.only(top: 10, right: 10, left: 10, bottom: 10);
  }
}
