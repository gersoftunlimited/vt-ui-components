// 🐦 Flutter imports:
import 'package:flutter/material.dart';

class BorderStyles {
  static InputBorder outlineBorder2Radius40Transparent = OutlineInputBorder(
      borderSide: const BorderSide(width: 2.0, color: Colors.transparent),
      borderRadius: BorderRadius.circular(40));
}
