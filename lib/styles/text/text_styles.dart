// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/app_colors/app_colors.dart';

// 🌎 Project imports:

/// Contains all the text styles constants
class TextStyles {
  // Text styles for common widgets
  static const TextStyle titleStyle = TextStyle(
      fontSize: 18,
      letterSpacing: 3,
      fontWeight: FontWeight.bold,
      color: AppColors.TEXT_COLOR);

  static const TextStyle titleDescription = TextStyle(
      fontSize: 15, fontWeight: FontWeight.normal, color: AppColors.TEXT_COLOR);

  static const TextStyle switchTitleStyle = TextStyle(
      color: AppColors.SWITCH_TITLE_COLOR,
      fontSize: 15,
      fontWeight: FontWeight.bold);

  static const TextStyle switchDescriptionStyle = TextStyle(
      color: AppColors.SWITCH_DESCRIPTION_COLOR,
      fontSize: 15,
      fontWeight: FontWeight.bold);

  static const TextStyle checkboxTitleStyle = TextStyle(
      color: AppColors.CHECKBOX_TITLE_COLOR,
      fontSize: 15,
      fontWeight: FontWeight.bold);

  static const TextStyle checkboxDescriptionStyle = TextStyle(
      color: AppColors.CHECKBOX_DESCRIPTION_COLOR,
      fontSize: 15,
      fontWeight: FontWeight.bold);

  static const TextStyle dateAcceptButtonTextStyle =
      TextStyle(fontSize: 15, color: AppColors.PRIMARY_COLOR);

  static const TextStyle dateCancelButtonTextStyle =
      TextStyle(fontSize: 15, color: AppColors.RED);

  static const TextStyle questionTextStyle = TextStyle(
      fontSize: 15.0,
      color: AppColors.NEWS_TITLE_TEXT_COLOR,
      fontWeight: FontWeight.bold);

  static const TextStyle answerTextStyle = TextStyle(
    fontSize: 14.0,
    color: AppColors.NEWS_TITLE_TEXT_COLOR,
  );

  static const TextStyle extraPageSectionTitle = TextStyle(
      fontSize: 18.0,
      fontWeight: FontWeight.bold,
      color: AppColors.PRIMARY_COLOR);

  static const TextStyle ticketDetailName = TextStyle(
      fontSize: 18.0,
      fontWeight: FontWeight.bold,
      color: AppColors.NEWS_TITLE_TEXT_COLOR);

  // End text styles for common widgets
}
