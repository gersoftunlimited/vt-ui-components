// 🐦 Flutter imports:
import 'package:flutter/material.dart';

class PaddingStyles {
  static getCardPadding() {
    return const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10);
  }
}
