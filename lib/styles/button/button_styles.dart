// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class ButtonStyles {
  // Button styles
  static ButtonStyle getButtonStyle(
      {MaterialStateProperty<EdgeInsetsGeometry?>? padding,
      MaterialStateProperty<OutlinedBorder?>? shape,
      MaterialStateProperty<Color?>? backgroundColor,
      MaterialStateProperty<Color?>? overlayColor,
      MaterialStateProperty<Color?>? foregroundColor}) {
    return ButtonStyle(
      padding: _getPadding(padding),
      shape: _getShape(shape),
      backgroundColor: _getBackgroundColor(backgroundColor),
      overlayColor: _getOverlayColor(overlayColor),
      foregroundColor: _getForegroundColor(foregroundColor),
    );
  }

  static ButtonStyle getDialogButtonStyle(
      {MaterialStateProperty<EdgeInsetsGeometry?>? padding,
      MaterialStateProperty<OutlinedBorder?>? shape,
      MaterialStateProperty<Color?>? backgroundColor,
      MaterialStateProperty<Color?>? overlayColor,
      MaterialStateProperty<Color?>? foregroundColor}) {
    return ButtonStyle(
      padding: _getPadding(padding),
      shape: _getDialogShape(shape),
      backgroundColor: _getDialogBackgroundColor(backgroundColor),
      overlayColor: _getdDialogOverlayColor(overlayColor),
      foregroundColor: _getDialogForegroundColor(foregroundColor),
    );
  }

  static ButtonStyle getDeleteDialogButtonStyle(
      {MaterialStateProperty<EdgeInsetsGeometry?>? padding,
      MaterialStateProperty<OutlinedBorder?>? shape,
      MaterialStateProperty<Color?>? overlayColor,
      MaterialStateProperty<Color?>? foregroundColor}) {
    return ButtonStyle(
      padding: _getPadding(padding),
      shape: _getDialogShape(shape),
      backgroundColor: _getDialogBackgroundColor(AppColors.RED),
      overlayColor: _getdDialogOverlayColor(overlayColor),
      foregroundColor: _getDialogForegroundColor(foregroundColor),
    );
  }

  // End of button styles

  static MaterialStateProperty<Color?>? _getForegroundColor(foregroundColor) {
    return foregroundColor ??
        MaterialStateProperty.resolveWith((state) {
          if (state.contains(MaterialState.pressed)) {
            return AppColors.BUTTON_PRESSED_TEXT_COLOR;
          }
          return AppColors.BUTTON_TEXT_COLOR;
        });
  }

  static _getPadding(padding) {
    return padding ??
        MaterialStateProperty.all(
            const EdgeInsets.only(top: 15, bottom: 15, left: 20, right: 20));
  }

  static _getShape(shape) {
    return shape ??
        MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
        );
  }

  static _getBackgroundColor(backgroundColor) {
    return backgroundColor ?? MaterialStateProperty.all(AppColors.WHITE);
  }

  static _getOverlayColor(overlayColor) {
    return overlayColor ??
        MaterialStateProperty.all(AppColors.BUTTON_PRESSED_BACKGROUND_COLOR);
  }

  static MaterialStateProperty<Color?>? _getDialogForegroundColor(
      foregroundColor) {
    return foregroundColor ??
        MaterialStateProperty.resolveWith((state) {
          if (state.contains(MaterialState.pressed)) {
            return AppColors.PRIMARY_COLOR;
          }
          return AppColors.WHITE;
        });
  }

  static _getDialogBackgroundColor(backgroundColor) {
    return backgroundColor ??
        MaterialStateProperty.all(AppColors.PRIMARY_COLOR);
  }

  static _getdDialogOverlayColor(overlayColor) {
    return overlayColor ?? MaterialStateProperty.all(Colors.white);
  }

  static _getDialogShape(shape) {
    return shape ??
        MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              side: const BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(50.0)),
        );
  }

  static getButtonTextStyle({TextStyle? textStyle}) {
    return textStyle ??
        const TextStyle(
          fontWeight: FontWeight.bold,
        );
  }
}
