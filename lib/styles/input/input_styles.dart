// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class InputStyles {
  static InputDecoration getInputDecoration(
      {String? hint,
      Widget? prefixIcon,
      Widget? suffixIcon,
      EdgeInsets? padding,
      bool showHintInLabel = true,
      bool showHintWhenFocus = false,
      bool isCollapsed = false}) {
    return InputDecoration(
      focusedBorder: BorderStyles.outlineBorder2Radius40Transparent,
      border: BorderStyles.outlineBorder2Radius40Transparent,
      enabledBorder: BorderStyles.outlineBorder2Radius40Transparent,
      disabledBorder: BorderStyles.outlineBorder2Radius40Transparent,
      contentPadding: padding ??
          const EdgeInsets.only(
              top: 20,
              bottom: 10.0,
              left: 20.0,
              right: 20.0 // HERE THE IMPORTANT PART
              ),
      filled: true,
      suffixIcon: suffixIcon,
      hintText: showHintWhenFocus ? hint : null,
      label: showHintInLabel
          ? Text(
              hint ?? "",
              maxLines: 1,
            )
          : null,
      prefixIcon: prefixIcon,
      alignLabelWithHint: true,
      hintStyle: const TextStyle(color: AppColors.INPUT_HINT_COLOR),
      fillColor: AppColors.INPUT_FILL_COLOR,
      isCollapsed: isCollapsed
    );
  }
}
