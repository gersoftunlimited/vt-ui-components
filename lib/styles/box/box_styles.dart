// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/app_colors/app_colors.dart';

// 🌎 Project imports:

/// Contains all the box styles
class BoxStyles {
  static BoxDecoration boxRadius50WithAlphaAndShadow = BoxDecoration(
      borderRadius: const BorderRadius.all(
        Radius.circular(50.0),
      ),
      boxShadow: [
        BoxShadow(
          color: AppColors.BLACK.withAlpha(20),
          spreadRadius: 5,
          blurRadius: 5,
          offset: const Offset(0, 3),
        )
      ]);

  static BoxDecoration boxRadius100WithAlphaAndShadow = BoxDecoration(
      borderRadius: const BorderRadius.all(
        Radius.circular(10.0),
      ),
      boxShadow: [
        BoxShadow(
          color: AppColors.BLACK.withAlpha(20),
          spreadRadius: 5,
          blurRadius: 5,
          offset: const Offset(0, 3),
        )
      ]);

  static BoxDecoration getBoxRadiusWithAlphaAndShadow({double radius = 10}) {
    return BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(radius),
        ),
        boxShadow: [
          BoxShadow(
            color: AppColors.BLACK.withAlpha(20),
            spreadRadius: 5,
            blurRadius: 5,
            offset: const Offset(0, 3),
          )
        ]);
  }

  static BoxDecoration simpleBoxWithShadow = BoxDecoration(
      color: AppColors.WHITE,
      borderRadius: BorderRadius.circular(10),
      boxShadow: [
        BoxShadow(
          color: AppColors.BLACK.withAlpha(20),
          spreadRadius: 5,
          blurRadius: 5,
          offset: const Offset(0, 3),
        )
      ]);

  static BoxDecoration extraTopBarItemSelected = BoxDecoration(
      color: AppColors.EXTRA_TOP_BAR_ITEM,
      boxShadow: [
        BoxShadow(
          color: AppColors.BLACK.withAlpha(20),
          spreadRadius: 5,
          blurRadius: 5,
          offset: const Offset(0, 3),
        )
      ],
      borderRadius: const BorderRadius.all(Radius.circular(20.0)));

  static BoxDecoration extraTopBarItemNotSelected = const BoxDecoration(
      color: AppColors.EXTRA_TOP_BAR_ITEM,
      borderRadius: BorderRadius.all(Radius.circular(20.0)));

  static BoxDecoration white = const BoxDecoration(color: AppColors.WHITE);
}
