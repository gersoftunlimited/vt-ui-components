// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/export.dart';

// 🌎 Project imports:

class ImageStyles {
  static BoxDecoration getInputDecoration(
      {Color? color = AppColors.WHITE, double radius = 50.0}) {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        boxShadow: [
          BoxShadow(
            color: AppColors.BLACK.withAlpha(20),
            spreadRadius: 2,
            blurRadius: 2,
            offset: const Offset(0, 0),
          )
        ],
        color: color);
  }
}
