// 🐦 Flutter imports:
import 'package:flutter/material.dart';
import 'package:vt_ui_components/app_colors/app_colors.dart';

// 🌎 Project imports:

class CardStyles {
  static BoxDecoration getCardDecoration() {
    return BoxDecoration(
      borderRadius: BorderRadius.circular(15),
      color: AppColors.WHITE,
      boxShadow: const [
        BoxShadow(
            color: AppColors.LIGHT_GREY,
            offset: Offset(0.0, 1.0),
            blurRadius: 6.0,
            spreadRadius: 1.0),
      ],
    );
  }
}
