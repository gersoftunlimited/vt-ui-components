library app_colors;

// ignore_for_file: constant_identifier_names

// 🐦 Flutter imports:
import 'package:flutter/material.dart';

class AppColors {
  static const Color PRIMARY_COLOR = Color(0xFF45B6CF);
  static const Color GREY = Color(0xFF828282);
  static const Color DARK_GREY = Color(0xFF3A3B3C);
  static const Color INPUT_HINT_COLOR = Color(0xFF999999);
  static const Color INPUT_FILL_COLOR = Color(0xFFF5F5F5);

  static const Color DEACTIVATED_COLOR = Color(0xFF305766);
  static const Color FLOATING_BUTTON_COLOR = Color(0xFF305766);

  static const Color BUTTON_TEXT_COLOR = Color(0xFF103D4E);
  static const Color BUTTON_PRESSED_TEXT_COLOR = WHITE;
  static const Color BUTTON_BACKGROUND_COLOR = WHITE;
  static const Color BUTTON_PRESSED_BACKGROUND_COLOR = PRIMARY_COLOR;
  static const Color LIGHT_GREY = Color(0xFFC0C0C0);
  static const Color SOFT_GREY = Color(0xFFEBECF0);

  static const Color TEXT_COLOR = Color(0xFF103D4E);
  static const Color SPACER_COLOR = LIGHT_GREY;

  static const Color BLACK = Color(0xFF000000);
  static const Color WHITE = Color(0xFFFFFFFF);
  static const Color RED = Color(0xFFFE4A49);
  static const Color GREEN = Color(0xFF008000);
  static const Color ORANGE = Color(0xFFFF9800);
  static const Color PINK = Color(0xFFFF0080);

  static const Color BLUE = Color(0xFF0000FF);
  static const Color ELECTRIC_BLUE = Color(0xFF00A7E1);

  static const Color DROPDOWN_ARROW_COLOR = Color(0xFF1F4959);

  static const Color SWITCH_TITLE_COLOR = Color(0xFF305766);
  static const Color SWITCH_DESCRIPTION_COLOR = PRIMARY_COLOR;
  static const Color SWITCH_ACTIVE_COLOR = PRIMARY_COLOR;

  static const Color CHECKBOX_TITLE_COLOR = Color(0xFF305766);
  static const Color CHECKBOX_DESCRIPTION_COLOR = PRIMARY_COLOR;
  static const Color CHECKBOX_ACTIVE_COLOR = PRIMARY_COLOR;

  static const Color APP_BAR_BACKGROUND_COLOR = Color(0xFFDCEEF3);

  static const Color TRANSPARENT = Color(0x00000000);

  static const Color EXTRA_TOP_BAR_ITEM = Color.fromRGBO(66, 167, 187, 1);
  static const Color NEWS_TITLE_TEXT_COLOR = Color.fromRGBO(18, 57, 70, 1);

  static const Color FOLDER_COLOR = Color.fromRGBO(255, 175, 46, 1);
}
